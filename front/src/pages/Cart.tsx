import { Wrapper } from 'components';
import { CartContainer, Layout } from 'containers';
import React from 'react';

export interface CartProps {}

const Cart: React.SFC<CartProps> = () => {
	return (
		<Layout>
			<Wrapper>
				<CartContainer />
			</Wrapper>
		</Layout>
	);
};

export default Cart;
