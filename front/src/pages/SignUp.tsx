import { Form, Wrapper } from 'components';
import { Layout } from 'containers';
import React, { useState } from 'react';

export interface SignUpProps {}

const SignUp: React.SFC<SignUpProps> = () => {
	const [user, setUser] = useState({
		first_name: '',
		last_name: '',
		email: '',
		password: '',
		confirm_password: '',
	});
	const isInvalid =
		!user.first_name ||
		!user.last_name ||
		!user.email ||
		!user.password ||
		!user.confirm_password;
	const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
	};
	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		const { name, value } = e.target;
		setUser({
			...user,
			[name]: value,
		});
	};

	return (
		<Layout>
			<Wrapper>
				<section>
					<Form>
						<Form.Title>Załóż konto</Form.Title>

						<Form.Base onSubmit={handleSubmit}>
							<Form.Input
								type='text'
								name='first_name'
								value={user.first_name}
								onChange={handleChange}
								placeholder='Imię'
							/>
							{/* {errors.first_name && (
								<Form.Error>{errors.first_name}</Form.Error>
							)} */}

							<Form.Input
								type='text'
								name='last_name'
								value={user.last_name}
								onChange={handleChange}
								placeholder='Nazwisko'
							/>
							{/* {errors.last_name && <Form.Error>{errors.last_name}</Form.Error>} */}

							<Form.Input
								type='email'
								name='email'
								value={user.email}
								onChange={handleChange}
								placeholder='Email'
							/>
							{/* {errors.city && <Form.Error>{errors.city}</Form.Error>} */}

							<Form.Input
								type='password'
								name='password'
								value={user.password}
								onChange={handleChange}
								placeholder='Hasło'
							/>
							{/* {errors.zip_code && <Form.Error>{errors.zip_code}</Form.Error>} */}
							<Form.Input
								type='password'
								name='confirm_password'
								value={user.confirm_password}
								onChange={handleChange}
								placeholder='Powtórz hasło'
							/>
							{/* {errors.zip_code && <Form.Error>{errors.zip_code}</Form.Error>} */}

							<Form.Submit disabled={isInvalid} type='submit'>
								Załóż konto
							</Form.Submit>
						</Form.Base>
					</Form>
				</section>
			</Wrapper>
		</Layout>
	);
};

export default SignUp;
