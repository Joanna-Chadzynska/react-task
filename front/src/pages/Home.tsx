import { Wrapper } from 'components';
import { Books, Layout } from 'containers';
import React from 'react';

const Home: React.SFC = () => (
	<Layout home>
		<Wrapper>
			<Books />
		</Wrapper>
	</Layout>
);

export default Home;
