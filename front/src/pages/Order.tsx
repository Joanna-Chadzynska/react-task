import { Layout, OrderContainer } from 'containers';
import React from 'react';

const Order: React.SFC = () => {
	return (
		<Layout>
			<OrderContainer />
		</Layout>
	);
};

export default Order;
