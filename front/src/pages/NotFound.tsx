import { ReactComponent as Image } from 'assets/icons/not_found_monitor.svg';
import { LostPage } from 'components';
import * as ROUTES from 'constants/routes';
import { Layout } from 'containers';
import React from 'react';

export interface NotFoundProps {}

const NotFound: React.SFC<NotFoundProps> = () => {
	return (
		<Layout notFound>
			<LostPage>
				<LostPage.Image>
					<Image />
				</LostPage.Image>

				<LostPage.Title>Ta strona nie istnieje!</LostPage.Title>
				<LostPage.Text>
					Wróć na stronę główną i spróbuj jeszcze raz.
				</LostPage.Text>
				<LostPage.Button to={ROUTES.HOME}>Zabierz mnie stąd</LostPage.Button>
			</LostPage>
		</Layout>
	);
};

export default NotFound;
