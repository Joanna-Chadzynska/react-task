import { ModalProvider } from 'contexts/modalContext';
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from 'routing/routes';
import { GlobalStyle } from './assets/styles/globalStyles';
import Theme from './assets/styles/theme';

const App = () => {
	return (
		<Theme>
			<GlobalStyle />
			<ModalProvider>
				<Router>
					<Routes />
				</Router>
			</ModalProvider>
		</Theme>
	);
};

export default App;
