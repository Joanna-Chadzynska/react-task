export const HOME = '/';
export const BOOK_DETAILS = '/book/:id';
export const ORDER = '/order';
export const CART = '/cart';
export const SIGN_IN = '/sign_in';
export const SIGN_UP = '/sign_up';
