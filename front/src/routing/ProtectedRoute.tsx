import React from 'react';
import { Route } from 'react-router-dom';

export interface ProtectedRouteProps {}

const ProtectedRoute: React.SFC<ProtectedRouteProps> = ({ ...restProps }) => {
	return <Route {...restProps}>{}</Route>;
};

export default ProtectedRoute;
