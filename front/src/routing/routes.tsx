import * as ROUTES from 'constants/routes';
import { Cart, Home, NotFound, Order, SignIn, SignUp } from 'pages';
import React from 'react';
import { Route, Switch } from 'react-router-dom';

const Routes: React.SFC = () => {
	return (
		<Switch>
			<Route exact path={ROUTES.HOME}>
				<Home />
			</Route>
			<Route exact path={ROUTES.CART}>
				<Cart />
			</Route>
			<Route exact path={ROUTES.ORDER}>
				<Order />
			</Route>
			<Route exact path={ROUTES.SIGN_IN}>
				<SignIn />
			</Route>
			<Route exact path={ROUTES.SIGN_UP}>
				<SignUp />
			</Route>
			<Route exact path='*'>
				<NotFound />
			</Route>
		</Switch>
	);
};

export default Routes;
