import React from 'react';
import { DefaultTheme, ThemeProvider } from 'styled-components';

const theme: DefaultTheme = {
	colors: {
		green: '#306B34',
		yellow: '#FCC71F',
		orange: '#EE8000',
		red: '#AC3931',
		gray: '#f4ededff',
		blue: ' #00395ce0',
	},
	body: {
		text: '#242325',
		lightText: '#9197AE',
		bg: '#e5f4f9',
	},
	fontFamily: {
		primary: "'Montserrat', sans-serif",
		secondary: "'Open Sans', sans-serif",
	},
};

const Theme: React.SFC = ({ children }) => (
	<ThemeProvider theme={theme}>{children}</ThemeProvider>
);

export default Theme;
