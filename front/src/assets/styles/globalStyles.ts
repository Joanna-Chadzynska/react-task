import { createGlobalStyle } from 'styled-components';
import { normalize } from 'styled-normalize';

export const GlobalStyle = createGlobalStyle`
    /* ${normalize}; */

    *,
    *::before,
    *::after {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    html, body {
        height: 100%;
    }

    body {
        background-color: ${({ theme }) => theme.body};
        color: ${({ theme }) => theme.body.text};
        display: flex;
        flex-direction: column;
        font-family: ${({ theme }) => theme.fontFamily.primary};
        line-height: 1.5;
        scroll-behavior: smooth;
        text-rendering: optimizeSpeed;
    }
    
    body, h1, h2, h3, h4, h5, h6, p, ol, ul {
        margin: 0;
        padding: 0;
        font-weight: normal;
    }

    body::-webkit-scrollbar {
        width: 0.65rem;
    }

    body::-webkit-scrollbar-track {
        background: #1e1e24;
    }

    body::-webkit-scrollbar-thumb {
        background: #6649b8;
    }

    #root {
        display: flex;
        flex-direction: column;
        height: 100%;
    }

    main {
        flex: 1 0 auto;
        padding: 10px;
    }


    ol, ul {
        list-style: none;
    }

    img {
        max-width: 100%;
        height: auto;
    }

	a {
        text-decoration: none;
     
	}


`;
