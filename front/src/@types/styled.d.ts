import 'styled-components';

declare module 'styled-components' {
	export interface DefaultTheme {
		colors: {
			green: string;
			yellow: string;
			orange: string;
			red: string;
			gray: string;
			blue: string;
		};
		body: {
			text: string;
			lightText: string;
			bg: string;
		};
		fontFamily: {
			primary: string;
			secondary: string;
		};
	}
}
