import { RootState } from 'app/store';
import { OrderElement } from 'models/interfaces/Order';
import { useSelector } from 'react-redux';

const useCartSummary = (order: OrderElement[]) => {
	const books = useSelector((state: RootState) => state.books.books);

	const orderedBooks =
		order &&
		order
			.map((order) =>
				books
					.filter((book) => book.id === order.id)
					.map((book) => {
						let item = {
							price: book.price,
							quantity: order.quantity,
							currency: book.currency,
							author: book.author,
							title: book.title,
							id: book.id,
							value: ((book.price * order.quantity) / 100)
								.toFixed(2)
								.replace('.', ','),
							cover: book.cover_url,
						};
						return item;
					})
			)
			.flat();

	const currency =
		order && [...new Set(orderedBooks.map((book) => book.currency))].toString();

	const booksPrice =
		order &&
		(orderedBooks.reduce((a: any, b) => b.price * b.quantity + a, 0) / 100)
			.toFixed(2)
			.replace('.', ',');

	const booksPriceWithCurrency = `${booksPrice} ${currency === 'PLN' && 'zł'}`;

	return { booksPrice, booksPriceWithCurrency, orderedBooks };
};

export default useCartSummary;
