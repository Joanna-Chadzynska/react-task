import { useEffect, useState } from 'react';

const useClickOutside = (ref: React.MutableRefObject<HTMLElement | null>) => {
	const [clickedOutside, setClickOutside] = useState(false);

	const handleClick = (e: any) => {
		// if (ref.current && ref.current.contains(e.target)) {
		// 	e.preventDefault();
		// 	e.stopPropagation();
		// 	setClickOutside(false);
		// } else {
		// 	setClickOutside(true);
		// }
	};

	useEffect(() => {
		document.addEventListener('click', handleClick);

		return () => document.removeEventListener('click', handleClick);
	}, []);

	return { clickedOutside };
};

export default useClickOutside;
