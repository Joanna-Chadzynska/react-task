import { useState } from 'react';

const usePagination = (data: any) => {
	const [currentPage, setCurrentPage] = useState(1);

	const maxPage = Math.ceil(data?.total_records / data.records_per_page);

	const next = () => {
		setCurrentPage((currentPage) => Math.min(currentPage + 1, maxPage));
	};

	const prev = () => {
		setCurrentPage((currentPage) => Math.max(currentPage - 1, 1));
	};

	const jump = (page: number) => {
		const pageNumber = Math.max(1, page);
		setCurrentPage((currentPage) => Math.min(pageNumber, maxPage));
	};

	const totalPages = Array.from({ length: maxPage }, (_, i) => i + 1);

	return { currentPage, setCurrentPage, next, prev, jump, maxPage, totalPages };
};

export default usePagination;
