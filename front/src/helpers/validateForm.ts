import { Order } from 'models/interfaces/Order';
import { Violations } from 'models/interfaces/Response';

const validateForm = (values: Order) => {
	const zipRegex = /\d{2}-\d{3}/;
	// first_name: length must be at least 4 characters long
	// last_name: length must be at least 5 characters long
	// city: is not allowed to be empty
	// zip_code: fails to match the required pattern: /\d{2}-\d{3}/

	let errors: Violations = {};

	if (!values.order) {
		console.log('order should be array');
	}

	if (!values.first_name) {
		errors.first_name = 'Imię jest wymagane';
	} else if (values.first_name.length < 4) {
		errors.first_name = 'Imię powinno składać się z minimum 4 znaków';
	}

	if (!values.last_name) {
		errors.last_name = 'Nazwisko jest wymagane';
	} else if (values.last_name.length < 5) {
		errors.last_name = 'Nazwisko powinno składać się z minimum 5 znaków';
	}

	if (!values.city) {
		errors.city = 'Pole miejscowość nie powinno być puste';
	}

	if (!values.zip_code) {
		errors.zip_code = 'Kod pocztowy jest wymagany';
	} else if (!zipRegex.test(values.zip_code)) {
		errors.zip_code = 'Niewłaściwy format kodu pocztowego';
	}

	// if (!values.password) {
	// 	errors.password = 'Password is required';
	// } else if (values.password.length < 8) {
	// 	errors.password = 'Password must be 8 or more characters';
	// }

	return errors;
};

export { validateForm };
