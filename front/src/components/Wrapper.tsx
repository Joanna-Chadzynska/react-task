import * as React from 'react';
import styled from 'styled-components/macro';

export const Container = styled.div`
	padding: 20px;
	margin-bottom: 4em;

	section {
		position: relative;
	}

	@media screen and (min-width: 600px) {
		display: grid;
		grid-template-columns: 1fr repeat(2, 4fr) 1fr;
		section {
			grid-column: 2 / 4;
		}
	}
`;

const Wrapper: React.SFC = ({ children, ...restProps }) => (
	<Container {...restProps}>{children}</Container>
);

export default Wrapper;
