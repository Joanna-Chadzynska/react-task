import styled from 'styled-components/macro';

export interface ItemProps {
	align?: 'left' | 'center' | 'right' | 'justify' | 'char' | undefined;
}

export const Container = styled.div`
	z-index: 9999;
`;

export const Inner = styled.table`
	border-collapse: collapse;
	border: 2px solid rgb(200, 200, 200);
	letter-spacing: 1px;
	table-layout: auto;
	width: 100%;
`;

export const Caption = styled.caption`
	font-weight: 600;
	padding: 10px;
	caption-side: top;
	text-transform: uppercase;
`;

export const BodyItem = styled.td<ItemProps>`
	text-align: ${({ align }) => align};
`;

export const HeaderItem = styled.th`
	&[scope='col'] {
		background-color: ${({ theme }) => theme.colors.blue};
		color: #fff;
	}
`;

export const Row = styled.tr`
	background-color: transparent;
	${HeaderItem}, ${BodyItem} {
		border: 1px solid rgb(190, 190, 190);
		font-size: 0.875rem;
		padding: 10px;
	}

	transition: background-color 0.15s ease-in;

	&:nth-child(even) {
		background-color: #eee;
	}

	&:hover {
		background-color: #b4dbf7;
	}
`;

export const Header = styled.thead``;

export const Body = styled.tbody``;

export const Footer = styled.tfoot`
	${Row} ${BodyItem} {
		border-top: 3px solid #949a9d;
		font-size: 1rem;
	}
`;
