import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Table } from 'components';
import React from 'react';

const orderedBooks = [
	{
		id: 457,
		title: 'Matematyka 1. Podręcznik. Zakres podstawowy',
		author: 'M. Karpiński, M. Dobrowolska, M. Braun, J. Lech',
		cover: '/static/cover/book/457.jpg',
		price: 3200,
		currency: 'PLN',
		quantity: 1,
		value: ((3200 * 1) / 100).toFixed(2).replace('.', ','),
	},
	{
		id: 458,
		title: 'Matematyka 1. Podręcznik. Zakres rozszerzony',
		author: 'M. Karpiński, M. Dobrowolska, M. Braun, J. Lech',
		cover: '/static/cover/book/458.jpg',
		price: 3300,
		currency: 'PLN',
		quantity: 1,
		value: ((3300 * 1) / 100).toFixed(2).replace('.', ','),
	},
	{
		id: 905,
		title: 'Nowa Matematyka z plusem 5. Podręcznik',
		author: 'M. Dobrowolska, M. Jucewicz, M. Karpiński, P. Zarzycki',
		cover: '/static/cover/book/905.jpg',
		price: 2990,
		currency: 'PLN',
		quantity: 1,
		value: ((2990 * 1) / 100).toFixed(2).replace('.', ','),
	},
];

describe('<Pagination/>', () => {
	it('renders the <Pagination/> component with populated data', () => {
		const { container, queryByText, queryAllByText } = render(
			<Theme>
				<Table>
					<Table.Caption>Zakupione produkty</Table.Caption>
					<Table.Header>
						<Table.Row>
							<Table.HeaderItem scope='col'>Tytuł</Table.HeaderItem>
							<Table.HeaderItem scope='col'>Autor</Table.HeaderItem>
							<Table.HeaderItem scope='col'>Ilość</Table.HeaderItem>
							<Table.HeaderItem scope='col'>Cena</Table.HeaderItem>
							<Table.HeaderItem scope='col'>Wartość</Table.HeaderItem>
						</Table.Row>
					</Table.Header>
					<Table.Body>
						{orderedBooks &&
							orderedBooks.map((book: any) => (
								<Table.Row key={book.id}>
									<Table.BodyItem>{book.title}</Table.BodyItem>
									<Table.BodyItem>{book.author}</Table.BodyItem>
									<Table.BodyItem>{book.quantity}szt.</Table.BodyItem>
									<Table.BodyItem align='right'>
										{(book.price / 100).toFixed(2).replace('.', ',')}
										{book.currency === 'PLN' && 'zł'}
									</Table.BodyItem>
									<Table.BodyItem align='right'>{`${book.value} ${
										book.currency === 'PLN' && 'zł'
									}`}</Table.BodyItem>
								</Table.Row>
							))}
					</Table.Body>
					<Table.Footer>
						<Table.Row>
							<Table.BodyItem align='right' colSpan={5}>
								Razem: <strong>29,90 zł</strong>
							</Table.BodyItem>
						</Table.Row>
					</Table.Footer>
				</Table>
			</Theme>
		);

		expect(queryByText('Zakupione produkty')).toBeTruthy();
		expect(queryByText('Tytuł')).toBeTruthy();
		expect(queryByText('Cena')).toBeTruthy();
		expect(queryAllByText('29,90 zł')).toBeTruthy();
		expect(queryByText('Nowa Matematyka z plusem 5. Podręcznik')).toBeTruthy();
		expect(
			queryByText('Matematyka 1. Podręcznik. Zakres rozszerzony')
		).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
