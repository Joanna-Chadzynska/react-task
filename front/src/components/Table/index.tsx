import React from 'react';
import {
	Body,
	BodyItem,
	Caption,
	Container,
	Footer,
	Header,
	HeaderItem,
	Inner,
	Row,
} from './styles/table';

export interface TableProps {}

export interface ItemProps {
	colSpan?: number;
	align?: 'left' | 'center' | 'right' | 'justify' | 'char' | undefined;
	scope?: 'row' | 'col';
}

export interface TableComposition {
	Body: React.FC;
	BodyItem: React.FC<ItemProps>;
	Caption: React.FC;
	Footer: React.FC;
	Header: React.FC;
	HeaderItem: React.FC<ItemProps>;
	Row: React.FC;
}

const Table: React.SFC<TableProps> & TableComposition = ({
	children,
	...restProps
}) => {
	return (
		<Container {...restProps}>
			<Inner>{children}</Inner>
		</Container>
	);
};

export const TableCaption: React.FC = ({ children, ...restProps }) => (
	<Caption {...restProps}>{children}</Caption>
);

export const TableHeader: React.FC = ({ children, ...restProps }) => (
	<Header {...restProps}>{children}</Header>
);

export const TableBody: React.FC = ({ children, ...restProps }) => (
	<Body {...restProps}>{children}</Body>
);

export const TableFooter: React.FC = ({ children, ...restProps }) => (
	<Footer {...restProps}>{children}</Footer>
);

export const TableBodyItem: React.FC<ItemProps> = ({
	colSpan,
	align,
	scope,
	children,
	...restProps
}) => (
	<BodyItem scope={scope} align={align} colSpan={colSpan} {...restProps}>
		{children}
	</BodyItem>
);

export const TableHeaderItem: React.FC<ItemProps> = ({
	scope,
	children,
	...restProps
}) => (
	<HeaderItem scope={scope} {...restProps}>
		{children}
	</HeaderItem>
);

export const TableRow: React.FC = ({ children, ...restProps }) => (
	<Row {...restProps}>{children}</Row>
);

Table.Body = TableBody;
Table.BodyItem = TableBodyItem;
Table.Caption = TableCaption;
Table.Footer = TableFooter;
Table.Header = TableHeader;
Table.HeaderItem = TableHeaderItem;
Table.Row = TableRow;

export default Table;
