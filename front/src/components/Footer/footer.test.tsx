import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Footer } from 'components';
import React from 'react';

describe('<Footer/>', () => {
	it('renders the <Footer/> with populated data', () => {
		const { container, getByText } = render(
			<Theme>
				<Footer>
					Jo Biden
					<span>{new Date().getFullYear()}</span>
				</Footer>
			</Theme>
		);

		expect(getByText('Jo Biden')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
