import React from 'react';
import { Container, Group, Icon, Link } from './styles/footer';

export interface FooterComposition {
	Group: React.FC;
	Link: React.FC<LinkProps>;
}

export interface LinkProps {
	path: string;
	icon?: string;
}

const Footer: React.SFC & FooterComposition = ({ children, ...restProps }) => (
	<Container {...restProps}>{children}</Container>
);

const FooterGroup: React.FC = ({ children, ...restProps }) => (
	<Group {...restProps}>{children}</Group>
);

const FooterLink: React.FC<LinkProps> = ({
	children,
	path,
	icon,
	...restProps
}) => (
	<Link href={path} target='_blank' rel='noopener noreferrer' {...restProps}>
		<Icon src={`images/icons/${icon}.svg`} alt={`${icon} link`}>
			{children}
		</Icon>
	</Link>
);

Footer.Group = FooterGroup;
Footer.Link = FooterLink;

export default Footer;
