import styled from 'styled-components/macro';

export const Container = styled.footer`
	background-color: ${({ theme }) => theme.colors.red};
	display: flex;
	justify-content: center;
	flex-direction: column;
	align-items: center;
	flex-shrink: 0;
	height: 120px;
	margin-top: -50px;

	span {
		color: #ffffff;
		font-size: clamp(0.875rem, 5vw, 1.1rem);
		font-weight: 500;
	}
`;

export const Link = styled.a`
	width: 40px;
	height: 40px;
	transition: transform 0.25s ease-in-out;

	&:hover {
		transform: scale(1.2);
	}
`;

export const Group = styled.div`
	color: #fff;
	display: flex;
	margin-top: 1em;

	${Link} + ${Link} {
		margin-left: 1.5em;
	}
`;

export const Icon = styled.img`
	max-width: 100%;
	filter: invert(1);
`;
