export interface CardProps {
	width?: string;
}

export interface ImageProps {
	src: string;
	alt?: string;
}

export interface InputProps {
	onChange?: (e: React.ChangeEvent<HTMLInputElement>) => void;
	value?: string | number | readonly string[] | undefined;
	type: 'number' | 'text' | 'date' | 'radio' | 'checkbox' | 'search';
	id?: string;
	name?: string;
	searchType?: string;
	checked?: boolean;
	placeholder?: string;
}

export interface CardComposition {
	AddToCart: React.FC<CardProps>;
	Author: React.FC;
	Content: React.FC;
	Currency: React.FC;
	Description: React.FC<CardProps>;
	Group: React.FC;
	GroupContainer: React.FC;
	Image: React.FC<ImageProps>;
	Input: React.FC<InputProps>;
	Search: React.FC<InputProps>;
	Price: React.FC;
	PriceWrapper: React.FC;
	Text: React.FC;
	Title: React.FC;
}
