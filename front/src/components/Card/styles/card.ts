import styled from 'styled-components/macro';

export interface CardProps {
	width?: string;
}

export const GroupContainer = styled.section`
	display: flex;
	flex-direction: column;
	margin-top: 3em;
	@media screen and (min-width: 800px) {
		margin-top: 0;
	}
`;

export const Group = styled.div`
	margin-bottom: 3rem;
	position: relative;
	display: grid;
	grid-gap: 2em;

	@media screen and (min-width: 600px) {
		grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
	}
`;

export const SearchContainer = styled.div`
	display: flex;
	align-items: center;
	flex-direction: column;
	position: relative;
	margin-bottom: 2em;
	padding: 0;
	height: 40px;
	@media screen and (min-width: 800px) {
		align-self: center;
		width: 330px;
	}
`;

export const Search = styled.input`
	border: 1px solid #ccc;
	border-radius: 3px;
	background-color: ${({ theme }) => theme.colors.gray};
	box-shadow: 0px 8px 15px rgba(0, 0, 0, 0.1);
	width: 100%;
	height: 100%;
	padding: 10px 20px;
	font-size: clamp(1rem, 5vw, 1.15rem);
	&:focus {
		border: 2px solid ${({ theme }) => theme.colors.red};
		border-radius: 3px;
		outline: none;
	}

	&::placeholder {
		font-size: 1rem;
	}
`;

export const SearchInputGroup = styled.div`
	display: flex;
	justify-content: center;
	width: 100%;
	div {
		display: flex;
		align-items: center;
		margin-right: 1em;

		input[type='radio'] {
			margin: 0 0.5em 0 0;
			padding: 0;
			min-width: 0;
		}
	}
`;

export const SearchButton = styled.button`
	background: transparent;
	border: none;
	cursor: pointer;
	position: absolute;
	right: 0.75rem;
	top: 0.5rem;
	span {
		display: block;
		width: 20px;
		height: 20px;
		border: 3px solid ${({ theme }) => theme.colors.red};
		border-radius: 50%;
		position: relative;

		&::after {
			content: '';
			display: block;
			width: 4px;
			height: 10px;
			background: ${({ theme }) => theme.colors.red};
			border-radius: 3px;
			position: absolute;
			transform: rotate(-45deg);
			bottom: -9px;
			right: -2px;
		}
	}
`;

export const Container = styled.div<CardProps>`
	background-color: ${({ theme }) => theme.colors.gray};
	box-shadow: 0 0 20px #aaa;
	box-shadow: 0.125em 0.25em 0.325em rgba(0, 0, 0, 0.2);
	border-radius: 10px;
	padding: 1.5em;
	display: grid;
	grid-template-rows: 1fr auto;
	@media screen and (min-width: 600px) {
	}
`;

export const Image = styled.img`
	border-radius: 2px;
	box-shadow: 0.25em 0.35em 1em rgba(0, 0, 0, 0.6);
	display: block;
	margin: 0 auto;
	max-width: 100%;
	object-fit: cover;
	position: relative;
	transition: transform 0.3s ease-in-out;
	&:hover {
		transform: scale(1.04);
	}
`;

export const AddToCartContainer = styled.div<CardProps>`
	display: flex;
	width: 100%;
`;

export const Input = styled.input`
	font-size: 1.1rem;
	padding: 0.525em 1em;
	min-width: 4em;
	margin-right: 2em;
`;

export const DescriptionContainer = styled.div<CardProps>`
	display: flex;
	flex-direction: column;
	justify-content: space-evenly;
`;

export const Title = styled.h2`
	display: block;
	display: -webkit-box;
	font-weight: 700;
	font-size: clamp(1em, 5vw, 1.25em);
	line-height: 1.6;
	max-width: clamp(200px, 50vw, 250px);
	overflow: hidden;
	text-overflow: ellipsis;
	-webkit-box-orient: vertical;
	-webkit-line-clamp: 2;
`;

export const Author = styled.p`
	color: #985403;
	font-size: clamp(0.725rem, 5vw, 0.9rem);
	font-weight: 600;
	line-height: 1.5;
	margin-bottom: 0.5em;
`;

export const Text = styled.p`
	font-size: 0.875em;
`;

export const PriceWrapper = styled.div`
	display: flex;
	gap: 0.25em;
	align-items: baseline;
	h2 {
		font-weight: bold;
	}
`;

export const Price = styled.h2``;

export const Currency = styled.h2`
	font-size: 1.2em;
`;

export const ContentWrapper = styled.div`
	display: grid;
	grid-template-rows: 1fr minmax(200px, 250px);
`;
