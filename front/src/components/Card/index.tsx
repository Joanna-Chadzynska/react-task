import React from 'react';
import {
	AddToCartContainer,
	Author,
	CardProps,
	Container,
	ContentWrapper,
	Currency,
	DescriptionContainer,
	Group,
	GroupContainer,
	Image,
	Input,
	Price,
	PriceWrapper,
	Search,
	SearchButton,
	SearchContainer,
	SearchInputGroup,
	Text,
	Title,
} from './styles/card';
import { CardComposition, ImageProps, InputProps } from './types';

const Card: React.SFC<CardProps> & CardComposition = ({
	children,
	width,
	...restProps
}) => (
	<Container width={width} {...restProps}>
		{children}
	</Container>
);

export const CardImage: React.FC<ImageProps> = ({ src, alt, ...restProps }) => (
	<Image src={src} alt={alt} {...restProps} />
);

export const CardGroup: React.FC = ({ children, ...restProps }) => (
	<Group {...restProps}>{children}</Group>
);
export const CardGroupContainer: React.FC = ({ children, ...restProps }) => (
	<GroupContainer {...restProps}>{children}</GroupContainer>
);

export const CardContent: React.FC = ({ children, ...restProps }) => (
	<ContentWrapper {...restProps}>{children}</ContentWrapper>
);

export const CardDescription: React.FC<CardProps> = ({
	width,
	children,
	...restProps
}) => (
	<DescriptionContainer width={width} {...restProps}>
		{children}
	</DescriptionContainer>
);

export const CardTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

export const CardAuthor: React.FC = ({ children, ...restProps }) => (
	<Author {...restProps}>{children}</Author>
);

export const CardText: React.FC = ({ children, ...restProps }) => (
	<Text {...restProps}>{children}</Text>
);

export const CardAddToCart: React.FC<CardProps> = ({
	children,
	...restProps
}) => <AddToCartContainer {...restProps}>{children}</AddToCartContainer>;

export const CardInput: React.FC<InputProps> = ({
	onChange,
	value,
	type,
	id,
	name,
	checked,
	placeholder,
	...restProps
}) => (
	<Input
		type={type}
		min='1'
		step='1'
		value={value}
		onChange={onChange}
		id={id}
		name={name}
		checked={checked}
		placeholder={placeholder}
		{...restProps}
	/>
);

export const CardSearch: React.FC<InputProps> = ({
	onChange,
	value,
	type,
	children,
	searchType,
	placeholder,
	...restProps
}) => (
	<SearchContainer>
		<Search
			type={type}
			value={value}
			onChange={onChange}
			placeholder={placeholder}
			aria-label={`Szukaj wg ${searchType === 'title' ? 'tytułu' : 'autora'}`}
			{...restProps}
		/>
		<SearchButton type='submit' role='button'>
			<span role='button'></span>
		</SearchButton>
		<SearchInputGroup>{children}</SearchInputGroup>
	</SearchContainer>
);

export const CardPriceWrapper: React.FC = ({ children, ...restProps }) => (
	<PriceWrapper {...restProps}>{children}</PriceWrapper>
);

export const CardPrice: React.FC = ({ children, ...restProps }) => (
	<Price {...restProps}>{children}</Price>
);

export const CardCurrency: React.FC = ({ children, ...restProps }) => (
	<Currency {...restProps}>{children}</Currency>
);

Card.AddToCart = CardAddToCart;
Card.Author = CardAuthor;
Card.Content = CardContent;
Card.Currency = CardCurrency;
Card.Description = CardDescription;
Card.Group = CardGroup;
Card.GroupContainer = CardGroupContainer;
Card.Image = CardImage;
Card.Input = CardInput;
Card.Price = CardPrice;
Card.PriceWrapper = PriceWrapper;
Card.Search = CardSearch;
Card.Text = CardText;
Card.Title = CardTitle;

export default Card;
