// import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Card } from 'components';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

const books = [
	{
		id: 457,
		title: 'Matematyka 1. Podręcznik. Zakres podstawowy',
		author: 'M. Karpiński, M. Dobrowolska, M. Braun, J. Lech',
		cover_url: '/static/cover/book/457.jpg',
		pages: 280,
		price: 3200,
		currency: 'PLN',
	},
	{
		id: 458,
		title: 'Matematyka 1. Podręcznik. Zakres rozszerzony',
		author: 'M. Karpiński, M. Dobrowolska, M. Braun, J. Lech',
		cover_url: '/static/cover/book/458.jpg',
		pages: 300,
		price: 3300,
		currency: 'PLN',
	},
	{
		id: 905,
		title: 'Nowa Matematyka z plusem 5. Podręcznik',
		author: 'M. Dobrowolska, M. Jucewicz, M. Karpiński, P. Zarzycki',
		cover_url: '/static/cover/book/905.jpg',
		pages: 256,
		price: 2990,
		currency: 'PLN',
	},
	{
		id: 1227,
		title: 'Język polski 6. Między nami. Podręcznik',
		author: 'A. Łuczak, A. Murdzek',
		cover_url: '/static/cover/book/1227.jpg',
		pages: 344,
		price: 3230,
		currency: 'PLN',
	},
	{
		id: 1228,
		title: 'Nowa Matematyka z plusem 6. Podręcznik',
		author: 'M. Dobrowolska, M. Jucewicz, M. Karpiński, P. Zarzycki',
		cover_url: '/static/cover/book/1228.jpg',
		pages: 268,
		price: 3190,
		currency: 'PLN',
	},
	{
		id: 1246,
		title: 'Matematyka z plusem 7. Podręcznik',
		author: 'praca zbiorowa pod redakcją M. Dobrowolskiej',
		cover_url: '/static/cover/book/1246.jpg',
		pages: 336,
		price: 3420,
		currency: 'PLN',
	},
];

describe('<Card/>', () => {
	it('renders single <Card/> component with populated data', () => {
		const { container, queryByText } = render(
			<MemoryRouter>
				<Theme>
					<Card>
						<Card.Content>
							<Card.Image src='/cover.jpg' />

							<Card.Description>
								<Card.Title>Nowa Matematyka z plusem 5. Podręcznik</Card.Title>
								<Card.Author>
									M. Dobrowolska, M. Jucewicz, M. Karpiński, P. Zarzycki
								</Card.Author>
								<Card.Text>Strony: 256</Card.Text>
								<Card.PriceWrapper>
									<Card.Price>29,90 zł</Card.Price>
									<Card.Currency>PLN</Card.Currency>
								</Card.PriceWrapper>
							</Card.Description>
						</Card.Content>

						<Card.AddToCart>
							<Card.Input value='2' onChange={() => {}} />
							<Card.Button onClick={() => {}}>
								Dodaj do koszyka
								<img src='/addToCart.svg' alt='add to cart icon' />
							</Card.Button>
						</Card.AddToCart>
					</Card>
				</Theme>
			</MemoryRouter>
		);

		expect(queryByText('Nowa Matematyka z plusem 5. Podręcznik')).toBeTruthy();
		expect(
			queryByText('M. Dobrowolska, M. Jucewicz, M. Karpiński, P. Zarzycki')
		).toBeTruthy();
		expect(queryByText('Strony: 256')).toBeTruthy();
		expect(queryByText('Dodaj do koszyka')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
	it('renders <Card/> component in group  with populated data', () => {
		const { container, queryByText, getAllByText } = render(
			<MemoryRouter>
				<Theme>
					<Card.Group>
						{books.map((book) => (
							<Card key={book.id}>
								<Card.Content>
									<Card.Description>
										<Card.PriceWrapper>
											<Card.Price>
												{(book.price / 100).toFixed(2).replace('.', ',')}
											</Card.Price>
											<Card.Currency>
												{book.currency === 'PLN' && 'zł'}
											</Card.Currency>
										</Card.PriceWrapper>
									</Card.Description>
								</Card.Content>
							</Card>
						))}
					</Card.Group>
				</Theme>
			</MemoryRouter>
		);

		expect(getAllByText('zł')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
