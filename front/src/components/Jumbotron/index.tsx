import banner from 'assets/banner/banner.svg';
import React from 'react';
import {
	Container,
	Content,
	Image,
	Inner,
	SubTitle,
	Title,
} from './styles/jumbotron';

export interface JumbotronProps {}
export interface ImageProps {
	src?: string;
}
export interface JumbotronComposition {
	Image: React.FC<ImageProps>;
	Content: React.FC;
	Title: React.FC;
	SubTitle: React.FC;
}

const Jumbotron: React.SFC<JumbotronProps> & JumbotronComposition = ({
	children,
	...restProps
}) => {
	return (
		<Container {...restProps}>
			<Inner>{children}</Inner>
		</Container>
	);
};

export const JumbotronImage: React.FC<ImageProps> = ({
	children,

	...restProps
}) => <Image src={banner} {...restProps}></Image>;

export const JumbotronContent: React.FC = ({ children, ...restProps }) => (
	<Content {...restProps}>{children}</Content>
);

export const JumbotronSubTitle: React.FC = ({ children, ...restProps }) => (
	<SubTitle {...restProps}>{children}</SubTitle>
);

export const JumbotronTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

Jumbotron.Content = JumbotronContent;
Jumbotron.Image = JumbotronImage;
Jumbotron.SubTitle = JumbotronSubTitle;
Jumbotron.Title = JumbotronTitle;

export default Jumbotron;
