import { Container as Button } from 'components/Button/styles/button';
import styled from 'styled-components/macro';

export const Container = styled.div`
	background-color: ${({ theme }) => theme.colors.gray};
	width: 100%;
	height: 70vh;
	margin-top: 4.625rem;
	height: 100%;

	@media screen and (min-width: 800px) {
		height: 70vh;
		margin-top: 0;
		display: grid;
		grid-template-columns: 1fr repeat(2, 4fr) 1fr;
	}
`;

export const Inner = styled.div`
	padding: 20px;
	margin-top: 4em;
	display: flex;
	flex-direction: column-reverse;
	justify-content: space-between;

	@media screen and (min-width: 800px) {
		grid-column: 2 / 4;
		flex-direction: row;
		margin-top: 0;
	}
`;

export const Image = styled.img`
	max-width: 100%;
	padding: 1em;
	@media screen and (min-width: 800px) {
		grid-column: 3 / 4;
		width: 53%;
		padding: 0;
	}
`;

export const Content = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: center;
	line-height: 1.6;
	margin-top: 2em;
	margin-bottom: -2em;
	${Button} {
		align-self: flex-start;
		padding: 0.75em 1.75em;
		font-weight: 600;
	}
	@media screen and (min-width: 800px) {
		grid-column: 2 / 3;
		width: 35%;
		margin-top: 0;
		margin-bottom: 0;
	}
`;

export const Title = styled.h1`
	font-weight: 700;
	font-size: clamp(2.25rem, 5vw, 3.125rem);
	white-space: nowrap;
	margin: 0.2em 0;
`;

export const SubTitle = styled.h3`
	font-weight: 500;
	font-size: clamp(1rem, 5vw, 1.5rem);
	margin: 0 0 2em 0;
`;
