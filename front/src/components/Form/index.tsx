import React from 'react';
import {
	Base,
	Container,
	Error,
	Input,
	Label,
	Submit,
	Text,
	TextSmall,
	Title,
} from './styles/form';

export interface FormProps {}

export interface LinkProps {
	to?: any;
}

export interface SubmitProps {
	disabled?: boolean;
	type?: 'button' | 'submit' | 'reset' | undefined;
}

export interface BaseProps {
	onSubmit: (e: React.FormEvent<HTMLFormElement>) => void;
}

export interface InputProps {
	type: 'text' | 'number' | 'email' | 'password';
	name: string;
	value: any;
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
	placeholder?: string;
	id?: string;
	hasError?: boolean;
}

export interface FormComposition {
	Base: React.FC<BaseProps>;
	Error: React.FC;
	Title: React.FC;
	Text: React.FC;
	TextSmall: React.FC;

	Input: React.FC<InputProps>;
	Label: React.FC;

	Submit: React.FC<SubmitProps>;
}

const Form: React.SFC<FormProps> & FormComposition = ({
	children,
	...restProps
}) => <Container {...restProps}>{children}</Container>;

export const FormBase: React.FC<BaseProps> = ({
	onSubmit,
	children,
	...restProps
}) => (
	<Base onSubmit={onSubmit} {...restProps}>
		{children}
	</Base>
);

export const FormError: React.FC = ({ children, ...restProps }) => (
	<Error {...restProps}>{children}</Error>
);

export const FormTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

export const FormText: React.FC = ({ children, ...restProps }) => (
	<Text {...restProps}>{children}</Text>
);

export const FormTextSmall: React.FC = ({ children, ...restProps }) => (
	<TextSmall {...restProps}>{children}</TextSmall>
);

export const FormInput: React.FC<InputProps> = ({
	type,
	name,
	value,
	onChange,
	placeholder,
	hasError,
	...restProps
}) => (
	<Input
		type={type}
		name={name}
		value={value}
		onChange={onChange}
		placeholder={placeholder}
		autoComplete='off'
		hasError={hasError}
		{...restProps}
	/>
);

export const FormLabel: React.FC = ({ children, ...restProps }) => (
	<Label {...restProps}>{children}</Label>
);

export const FormSubmit: React.FC<SubmitProps> = ({
	disabled,
	type,
	children,
	...restProps
}) => (
	<Submit disabled={disabled} type={type} {...restProps}>
		{children}
	</Submit>
);

Form.Base = FormBase;
Form.Error = FormError;
Form.Input = FormInput;
Form.Label = FormLabel;
Form.Submit = FormSubmit;
Form.Text = FormText;
Form.TextSmall = FormTextSmall;
Form.Title = FormTitle;

export default Form;
