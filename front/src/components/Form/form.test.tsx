import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Form } from 'components';
import React from 'react';

jest.mock('react-router-dom');

describe('<Form/>', () => {
	it('renders the <Form/> with populated data', () => {
		const { container, queryByText, getByPlaceholderText, getByText } = render(
			<Theme>
				<Form>
					<Form.Title>Złóż zamówienie</Form.Title>
					<Form.Base onSubmit={() => {}}>
						<Form.Input
							type='text'
							name='first_name'
							value='Kondzio'
							onChange={() => {}}
							placeholder='Imię'
							hasError={false}
						/>
						<Form.Label>Imię</Form.Label>

						<Form.Submit type='submit' disabled>
							Zamów
						</Form.Submit>
					</Form.Base>
					<Form.Text>Masz już konto?</Form.Text>

					<Form.TextSmall>
						This page is protected by Google reCAPTCHA to ensure you're not a
						bot. Learn more.
					</Form.TextSmall>
				</Form>
			</Theme>
		);

		expect(queryByText('Złóż zamówienie')).toBeTruthy();
		expect(queryByText('Masz już konto?')).toBeTruthy();
		expect(queryByText('Zamów')).toBeTruthy();
		expect(getByText('Zamów').disabled).toBeTruthy();
		expect(
			queryByText(
				"This page is protected by Google reCAPTCHA to ensure you're not a bot. Learn more."
			)
		).toBeTruthy();
		expect(getByPlaceholderText('Imię')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});

	it('renders the <Form/> with an error', () => {
		const { container, getByText } = render(
			<Theme>
				<Form>
					<Form.Error>Imię powinno zawierać minimum 4 znaki</Form.Error>

					<Form.Submit type='submit'>Złóż zamówienie</Form.Submit>
				</Form>
			</Theme>
		);

		expect(getByText('Imię powinno zawierać minimum 4 znaki')).toBeTruthy();
		expect(getByText('Złóż zamówienie')).toBeTruthy();
		expect(getByText('Złóż zamówienie').disabled).toBeFalsy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
