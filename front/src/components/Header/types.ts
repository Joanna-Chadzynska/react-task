export interface HeaderProps {}

export interface LinkProps {
	to?: any;
	src?: any;
}

export interface InputProps {
	type?: string;
	id?: string;
}

export interface BurgerProps {
	htmlFor?: string;
}
export interface NotificationProps {
	htmlFor?: string;
}

export interface SearchProps {
	searchTerm: string;
	setSearchTerm?: (target: string) => string;
	placeholder?: string;
}

export interface HeaderComposition {
	Badge: React.FC<NotificationProps>;
	Burger: React.FC<BurgerProps>;
	Group: React.FC;
	Input: React.FC<InputProps>;
	Link: React.FC<LinkProps>;
	Logo: React.FC<LinkProps>;
	Menu: React.FC;
	MenuItem: React.FC;
	Inner: React.FC;
}
