import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Header } from 'components';
import * as ROUTES from 'constants/routes';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

describe('<Header/>', () => {
	it('renders the <Header/> with populated data', () => {
		const { container, getByText } = render(
			<Theme>
				<MemoryRouter>
					<Header>
						<Header.Logo to={ROUTES.HOME} src='/logo.svg' />

						<Header.Input type='checkbox' id='nav-toggle' />

						<Header.Menu>
							<Header.Group>
								<Header.MenuItem>
									<Header.Link to={ROUTES.HOME}>Strona główna</Header.Link>
								</Header.MenuItem>
								<Header.MenuItem>
									<Header.Link to={ROUTES.CART}>
										Koszyk
										<Header.Badge>4</Header.Badge>
									</Header.Link>
								</Header.MenuItem>
							</Header.Group>
						</Header.Menu>

						<Header.Burger htmlFor='nav-toggle' />
					</Header>
				</MemoryRouter>
			</Theme>
		);

		expect(getByText('Strona główna')).toBeTruthy();
		expect(getByText('Koszyk')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
