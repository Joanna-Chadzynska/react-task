import { useClickOutside } from 'hooks';
import * as React from 'react';
import { useRef } from 'react';
import {
	Badge,
	Burger,
	Container,
	Group,
	Inner,
	Item,
	Link,
	LogoContainer,
	Navigation,
	NavToggle,
} from './styles/header';
import {
	BurgerProps,
	HeaderComposition,
	HeaderProps,
	InputProps,
	LinkProps,
	NotificationProps,
} from './types';

const Header: React.SFC<HeaderProps> & HeaderComposition = ({
	children,
	...restProps
}) => {
	const ref = useRef(null);
	const { clickedOutside } = useClickOutside(ref);

	return (
		<Container ref={ref} {...restProps}>
			{children}
		</Container>
	);
};

export const HeaderInner: React.FC = ({ children, ...restProps }) => (
	<Inner {...restProps}>{children}</Inner>
);

export const HeaderMenu: React.FC = ({ children, ...restProps }) => (
	<Navigation data-testid='nav' {...restProps}>
		{children}
	</Navigation>
);

export const HeaderBadge: React.FC<NotificationProps> = ({
	children,
	...restProps
}) => (
	<Badge data-testid='nav' {...restProps}>
		{children}
	</Badge>
);

export const HeaderMenuItem: React.FC = ({ children, ...restProps }) => (
	<Item data-testid='nav' {...restProps}>
		{children}
	</Item>
);

export const HeaderMenuLink: React.FC<LinkProps> = ({
	to,
	children,
	...restProps
}) => (
	<Link data-testid='nav' to={to} {...restProps}>
		{children}
	</Link>
);

export const HeaderBurger: React.FC<BurgerProps> = ({
	htmlFor,
	...restProps
}) => (
	<Burger htmlFor='nav-toggle' {...restProps} data-testid='nav'>
		<span data-testid='nav'></span>
	</Burger>
);

export const HeaderInput: React.FC<InputProps> = ({
	children,
	type,
	id,
	...restProps
}) => (
	<NavToggle type={type} id='nav-toggle' {...restProps}>
		{children}
	</NavToggle>
);

export const HeaderGroup: React.FC = ({ children, ...restProps }) => (
	<Group data-testid='nav' {...restProps}>
		{children}
	</Group>
);

export const HeaderLogo: React.FC<LinkProps> = ({ to, ...restProps }) => (
	<LogoContainer to='/' {...restProps}>
		<h1>
			Books<strong>Store</strong>
		</h1>
	</LogoContainer>
);

Header.Badge = HeaderBadge;
Header.Burger = HeaderBurger;
Header.Group = HeaderGroup;
Header.Inner = HeaderInner;
Header.Input = HeaderInput;
Header.Link = HeaderMenuLink;
Header.Logo = HeaderLogo;
Header.Menu = HeaderMenu;
Header.MenuItem = HeaderMenuItem;

export default Header;
