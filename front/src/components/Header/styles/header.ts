import { Link as ReachRouterLink } from 'react-router-dom';
import styled from 'styled-components/macro';

export interface HeaderProps {
	active?: boolean;
}

export const LogoContainer = styled(ReachRouterLink)`
	display: flex;
	align-items: center;
	height: 100%;
	margin-left: 1em;
	color: ${({ theme }) => theme.body.text};

	h1 {
		font-size: clamp(1.25rem, 5vw, 1.5rem);
		strong {
			margin: 0;
			padding: 0;
			color: ${({ theme }) => theme.colors.orange};
		}
	}

	@media screen and (min-width: 800px) {
		grid-column: 2 / 3;
		margin: 0;
	}
`;

export const Logo = styled.img`
	height: clamp(1.5rem, 10vw, 2.812rem);
	width: auto;
`;

export const Container = styled.header``;

export const Inner = styled.div`
	text-align: center;
	position: fixed;
	width: 100%;
	height: 8vh;
	z-index: 999;

	@media screen and (min-width: 800px) {
		display: grid;
		grid-template-columns: 1fr auto repeat(2, 3fr) 1fr;
		position: relative;
	}
`;

// navigation with logo

export const Navigation = styled.nav`
	background-color: #ffffff;
	position: absolute;
	right: 0;
	top: 100%;
	text-align: right;
	transform: scale(1, 0);
	transform-origin: top;
	transition: transform 0.4s ease-in-out;
	width: 100%;

	@media screen and (min-width: 800px) {
		all: unset;
		grid-column: 3 / 5;
		display: flex;
		align-items: center;
		justify-content: flex-end;
	}
`;

export const Group = styled.ul`
	margin: 0;
	padding: 0;

	@media screen and (min-width: 800px) {
		display: flex;
		align-items: center;
	}
`;

export const Item = styled.li`
	margin-bottom: 1em;
	margin-right: 1em;
	padding: 1rem 0;

	@media screen and (min-width: 800px) {
		margin-bottom: 0;
		margin-right: 0;
		padding: 0 1rem;
	}
`;

export const Burger = styled.label`
	position: absolute;
	top: 0;
	right: 0;
	margin-right: 1em;
	height: 100%;
	display: flex;
	align-items: center;

	span,
	span::before,
	span::after {
		display: block;
		background-color: ${({ theme }) => theme.colors.orange};
		height: 4px;
		width: 2em;
		border-radius: 4px;
		position: relative;
		transition: transform 220ms ease-in-out;
	}

	span::before,
	span::after {
		content: '';
		position: absolute;
	}

	span::before {
		top: -12px;
		transition: top 100ms 250ms ease-in, transform 220ms ease-in-out;
	}

	span::after {
		bottom: -12px;
		transition: bottom 100ms 250ms ease-in, transform 220ms ease-in-out;
	}

	@media screen and (min-width: 800px) {
		display: none;
	}
`;

export const Link = styled(ReachRouterLink)`
	display: inline-block;
	color: #6c7182;
	font-size: 1.2rem;
	font-weight: 600;
	opacity: 0;
	position: relative;
	transition: color 0.15s ease-in, opacity 0.15s ease-in-out;

	&:hover {
		color: ${({ theme }) => theme.body.text};
	}

	svg {
		fill: ${({ theme }) => theme.colors.orange};
		width: 44px;
		height: 40px;
		display: inline-block;
	}

	@media screen and (min-width: 800px) {
		opacity: 1;
		position: relative;

		&:hover::before {
			transform: scale(1, 1);
		}

		&::before {
			content: '';
			height: 5px;
			background-color: #000000;
			position: absolute;
			top: -0.75em;
			left: 0;
			right: 0;
			transform: scale(0, 1);
			transition: transform 0.25s ease-in-out;
		}
	}
`;

export const NavToggle = styled.input`
	position: absolute !important;
	top: -9999px !important;
	left: -9999px !important;

	&:focus ~ ${Burger} {
		outline: 3px solid rgba(lightblue, 0.75);
	}

	&:checked ~ ${Burger} {
		span {
			transform: rotate(225deg);
			transition: transform 220ms 120ms ease-in-out;
		}
		span::before {
			top: 0;
			transition: top 100ms ease-out;
		}
		span::after {
			bottom: 0;
			transform: rotate(-90deg);
			transition: bottom 100ms ease-out, transform 220ms 120ms ease-in-out;
		}
	}

	&:checked ~ ${Navigation} {
		transform: scale(1, 1);
	}

	&:checked ~ ${Navigation} ${Link} {
		opacity: 1;
		transition: opacity 0.25s ease-in-out 0.25s;
	}
`;

export const Badge = styled.span`
	position: absolute;
	top: -16px;
	right: -10px;
	padding: 0.312em 0.75em;
	border-radius: 50%;
	background: ${({ theme }) => theme.body.text};
	color: ${({ theme }) => theme.colors.gray};
	font-size: 0.875rem;

	@media screen and (min-width: 800px) {
		top: -10px;
		right: -11px;
	}
`;

// Search
export const Search = styled.div``;

export const SearchIcon = styled.button``;

export const SearchInput = styled.input<HeaderProps>``;
