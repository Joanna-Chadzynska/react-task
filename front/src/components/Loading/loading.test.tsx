import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Loading } from 'components';
import React from 'react';

describe('<Loading/>', () => {
	it('renders the <Loading/> component as Bouncer', () => {
		const { container } = render(
			<Theme>
				<Loading>
					<Loading.Bouncer />
				</Loading>
			</Theme>
		);

		expect(container.firstChild?.firstChild?.firstChild).toBeEmpty();
		expect(container.firstChild).toMatchSnapshot();
	});

	it('renders the <Loading/> component as Spinner', () => {
		const { container } = render(
			<Theme>
				<Loading>
					<Loading.Spinner />
				</Loading>
			</Theme>
		);

		expect(container.firstChild?.firstChild?.firstChild).toBeEmpty();
		expect(container.firstChild).toMatchSnapshot();
	});

	it('renders the <Loading/> component as Square', () => {
		const { container } = render(
			<Theme>
				<Loading>
					<Loading.Square />
				</Loading>
			</Theme>
		);

		expect(container.firstChild?.firstChild?.firstChild).toBeEmpty();
		expect(container.firstChild).toMatchSnapshot();
	});
});
