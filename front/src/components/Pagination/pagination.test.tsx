import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Pagination } from 'components';
import React from 'react';

describe('<Pagination/>', () => {
	it('renders the <Pagination/> component with populated data', () => {
		const { container, queryByText } = render(
			<Theme>
				<Pagination>
					<Pagination.Button onClick={() => {}} disabled={true}>
						Prev
					</Pagination.Button>
				</Pagination>
			</Theme>
		);

		expect(queryByText('Prev')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
