import React from 'react';
import { Button, Container } from './styles/pagination';

export interface PaginationProps {}

export interface ButtonProps {
	onClick?: () => void;
	disabled?: boolean;
	active?: boolean;
}

export interface PaginationComposition {
	Button: React.FC<ButtonProps>;
}

const Pagination: React.SFC<PaginationProps> & PaginationComposition = ({
	children,
	...restProps
}) => <Container {...restProps}>{children}</Container>;

export const PaginationButton: React.FC<ButtonProps> = ({
	onClick,
	active,
	children,
	disabled,
	...restProps
}) => (
	<Button disabled={disabled} onClick={onClick} active={active} {...restProps}>
		{children}
	</Button>
);

Pagination.Button = PaginationButton;
export default Pagination;
