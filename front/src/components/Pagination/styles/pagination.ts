import styled from 'styled-components/macro';

export interface ButtonProps {
	active?: boolean;
}

export const Container = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
`;

export const Button = styled.button<ButtonProps>`
	border: 1px solid transparent;
	border-radius: 2px;
	font-size: 0.875rem;
	padding: 0.5em 1em;
	transition: background-color 0.3s;

	&:hover {
		background-color: ${({ active, theme }) =>
			active ? theme.colors.yellow : '#ddd'};
	}

	${({ active, theme }) =>
		active
			? `background-color: ${theme.colors.orange};
            border-color: none;
            `
			: `background-color: #ffffff;
            border-color:#ddd;`}
`;
