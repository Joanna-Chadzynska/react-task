import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';

export const Container = styled.button`
	display: flex;
	align-items: center;
	justify-content: center;
	border: none;
	border-radius: 5px;
	background-color: ${({ theme }) => theme.colors.orange};
	color: #ffffff;
	font-family: ${({ theme }) => theme.fontFamily.primary};
	font-size: clamp(1rem, 5vw, 1.1rem);
	font-weight: 500;
	padding: 0.525em 0.875em;
	transition: background-color 0.3s ease-in-out;

	&:hover,
	&:focus {
		background-color: ${({ theme }) => theme.colors.red};
	}

	img {
		display: none;
	}

	@media screen and (min-width: 390px) {
		img {
			display: block;
			height: 2rem;
			margin-left: 1rem;
		}
	}
`;

export const ButtonLink = styled(Link)`
	background-color: ${({ theme }) => theme.colors.orange};
	border-radius: 5px;
	color: #ffffff;
	display: flex;
	justify-content: center;
	align-items: center;
	font-size: clamp(1.1rem, 5vw, 1.2rem);
	font-weight: 500;
	padding: 0.5em 1.2em;
	transition: background-color 0.3s ease-in-out;

	&:hover {
		background-color: ${({ theme }) => theme.colors.red};
	}
`;
