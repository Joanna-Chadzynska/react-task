import React from 'react';
import { ButtonLink, Container } from './styles/button';

export interface ButtonProps {
	onClick?: () => void;
}
export interface ButtonLinkProps {
	to?: any;
}
export interface ButtonComposition {
	Link: React.FC<ButtonLinkProps>;
}

const Button: React.SFC<ButtonProps> & ButtonComposition = ({
	children,
	onClick,
	...restProps
}) => {
	return (
		<Container onClick={onClick} {...restProps}>
			{children}
		</Container>
	);
};

export const ButtonWithLink: React.FC<ButtonLinkProps> = ({
	children,
	to,
	...restProps
}) => (
	<ButtonLink to={to} {...restProps}>
		{children}
	</ButtonLink>
);

Button.Link = ButtonWithLink;

export default Button;
