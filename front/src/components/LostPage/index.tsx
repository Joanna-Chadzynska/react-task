import React from 'react';
import { Button, Container, Image, Text, Title } from './styles/lostPage';

export interface LostPageComposition {
	Button: React.FC<LinkProps>;
	Image: React.FC<ImageProps>;
	Text: React.FC;
	Title: React.FC;
}

export interface LinkProps {
	to?: any;
}

export interface ImageProps {
	src?: any;
	alt?: string;
}

const LostPage: React.SFC & LostPageComposition = ({
	children,
	...restProps
}) => {
	return <Container {...restProps}>{children}</Container>;
};

export const LostPageButton: React.FC<LinkProps> = ({
	to,
	children,
	...restProps
}) => (
	<Button to={to} {...restProps}>
		{children}
	</Button>
);

export const LostPageText: React.FC = ({ children, ...restProps }) => (
	<Text {...restProps}>{children}</Text>
);

export const LostPageTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

export const LostPageImage: React.FC<ImageProps> = ({
	children,
	src,
	...restProps
}) => <Image {...restProps}>{children}</Image>;

LostPage.Button = LostPageButton;
LostPage.Image = LostPageImage;
LostPage.Text = LostPageText;
LostPage.Title = LostPageTitle;

export default LostPage;
