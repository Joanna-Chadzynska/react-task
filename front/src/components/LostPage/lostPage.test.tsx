import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { LostPage } from 'components';
import * as ROUTES from 'constants/routes';
import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

describe('<LostPage/>', () => {
	it('renders the <LostPage/> component with populated data', () => {
		const { container, queryByText } = render(
			<Theme>
				<Router>
					<LostPage>
						<LostPage.Image src='/notFound.svg' />

						<LostPage.Title>Ta strona nie istnieje!</LostPage.Title>
						<LostPage.Text>
							Wróć na stronę główną i spróbuj jeszcze raz.
						</LostPage.Text>
						<LostPage.Button to={ROUTES.HOME}>
							Zabierz mnie stąd
						</LostPage.Button>
					</LostPage>
				</Router>
			</Theme>
		);

		expect(queryByText('Ta strona nie istnieje!')).toBeTruthy();
		expect(
			queryByText('Wróć na stronę główną i spróbuj jeszcze raz.')
		).toBeTruthy();
		expect(queryByText('Zabierz mnie stąd')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
