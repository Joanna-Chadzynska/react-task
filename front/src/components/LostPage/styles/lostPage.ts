import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';

export const Container = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	justify-content: center;
	margin-top: 5em;
	width: 100%;
	padding: 3em 0;
	height: 80vh;

	@media screen and (min-width: 800px) {
		margin-top: 0;
	}
`;

export const Button = styled(Link)`
	background-color: inherit;
	border: 0;
	border-radius: 28px;
	border: solid 2px ${({ theme }) => theme.colors.orange};
	color: ${({ theme }) => theme.colors.orange};
	letter-spacing: 0.8px;
	line-height: 53px;
	font-size: clamp(1rem, 5vw, 1.25rem);
	padding: 0.25em 1em;
	text-align: center;
	transition: color, background-color 0.3s ease-in-out;

	&:hover,
	&:focus {
		background-color: ${({ theme }) => theme.colors.orange};
		color: #fff;
	}
`;

export const Title = styled.h1`
	/* color: #3eb8ea; */
	color: ${({ theme }) => theme.colors.orange};
	font-size: clamp(2rem, 5vw, 4.124rem);
	letter-spacing: 1px;
	text-transform: uppercase;
`;

export const Text = styled.p`
	font-size: clamp(0.875rem, 5vw, 1.25rem);
	letter-spacing: 1px;
	line-height: 1.6;
	color: #184a56;
	margin: 2em 0;
`;

export const Image = styled.div`
	min-width: 216px;
	margin-left: -20px;
	height: 400px;
	svg {
		max-width: 100%;
		width: 100%;
		height: 100%;
	}
`;
