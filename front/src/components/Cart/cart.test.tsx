// import '@testing-library/jest-dom/extend-expect';
import { render } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Cart } from 'components';
import * as ROUTES from 'constants/routes';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

describe('<Cart/>', () => {
	it('renders <Cart/> with populated data', () => {
		const { container, queryByText } = render(
			<MemoryRouter>
				<Theme>
					<Cart>
						<Cart.Title>
							Mój koszyk
							<Cart.Text>(2 produkt, 3 sztuka)</Cart.Text>
						</Cart.Title>
						<Cart.Break />
						<Cart.Group>
							<Cart.Inner></Cart.Inner>
						</Cart.Group>

						<Cart.Summary>
							<Cart.Title>PODSUMOWANIE</Cart.Title>
							<Cart.Text>
								DO ZAPŁATY: <strong>129,80 zł</strong>
							</Cart.Text>
							<Cart.LinksContainer>
								<Cart.Link to={ROUTES.HOME}>Wróć na stronę główną</Cart.Link>
								<Cart.Link to={ROUTES.ORDER}>Zamawiam i płacę</Cart.Link>
							</Cart.LinksContainer>
						</Cart.Summary>
					</Cart>
				</Theme>
			</MemoryRouter>
		);
		expect(queryByText('Mój koszyk')).toBeTruthy();
		expect(queryByText('PODSUMOWANIE')).toBeTruthy();
		expect(queryByText('Wróć na stronę główną')).toBeTruthy();
		expect(queryByText('Zamawiam i płacę')).toBeTruthy();
		expect(container.firstChild).toMatchSnapshot();
	});
});
