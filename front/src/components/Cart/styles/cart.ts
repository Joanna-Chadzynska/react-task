import { ButtonLink } from 'components/Button/styles/button';
import { Image } from 'components/Card/styles/card';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';

export const Container = styled.section`
	text-align: center;
	margin-top: 3em;
	@media screen and (min-width: 600px) {
		margin-top: 0;
		text-align: left;
	}
`;

export const Group = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	width: 100%;
`;

export const Title = styled.h2`
	font-size: clamp(1.5rem, 5vw, 1.825rem);
	line-height: 2;
`;

export const Inner = styled.div`
	display: grid;
	grid-template-rows: 1fr auto;
	grid-gap: 2em;
	justify-content: center;
	margin-top: 10em;
	text-align: center;
`;

export const Break = styled.div`
	margin: 1em 0;
`;

export const ItemLink = styled(Link)`
	background-color: ${({ theme }) => theme.colors.orange};
	border-radius: 5px;
	color: #ffffff;
	font-size: 1rem;
	padding: 0.5em 1.2em;
	text-transform: uppercase;
	transition: background-color 0.3s ease-in-out;

	&:hover {
		background-color: ${({ theme }) => theme.colors.yellow};
	}
`;

export const Text = styled.span`
	color: #949a9d;
`;

export const Summary = styled.div``;

export const SummaryLinksContainer = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	padding: 1em;
	${ButtonLink} {
		margin: 0 0.5em;
	}
	${Title} {
		font-size: clamp(1.25rem, 5vw, 1.5rem);
	}
`;

export const Feature = styled.div`
	background-color: ${({ theme }) => theme.colors.gray};
	box-shadow: 0 0 20px #aaa;
	box-shadow: 0.125em 0.25em 0.325em rgba(0, 0, 0, 0.2);
	border-radius: 10px;
	width: 100%;
	margin: 1em 0;
	padding: 1.5em;
	display: flex;
	flex-direction: column;
	justify-content: space-between;

	@media screen and (min-width: 600px) {
		flex-direction: row;
	}
`;

export const FeatureImage = styled(Image)`
	all: unset;
	border-radius: 5px;
	margin: 0;
	padding: 0;
	max-width: 100%;
	object-fit: contain;
	transition: transform 0.3s ease-in-out;
	margin-bottom: 1em;
	&:hover {
		transform: scale(1.04);
	}

	@media screen and (min-width: 600px) {
		max-width: 25%;
		max-height: 300px;
		margin-bottom: 0;
	}
`;

export const FeatureContent = styled.div`
	width: 100%;
	@media screen and (min-width: 600px) {
		width: 70%;
	}
`;

export const FeatureTitle = styled.h3`
	display: block;
	font-weight: 700;
	font-size: clamp(1.25rem, 5vw, 1.65rem);
	line-height: 1.6;
`;
