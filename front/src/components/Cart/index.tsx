import React from 'react';
import {
	Break,
	Container,
	Feature,
	FeatureContent,
	FeatureImage,
	FeatureTitle,
	Group,
	Inner,
	Summary,
	SummaryLinksContainer,
	Text,
	Title,
} from './styles/cart';

export interface CartProps {}

export interface LinkProps {
	to?: any;
}
export interface ImageProps {
	src?: string;
}

export interface CartComposition {
	Break: React.FC;
	Group: React.FC;
	Inner: React.FC;
	Feature: React.FC;
	FeatureContent: React.FC;
	FeatureImage: React.FC<ImageProps>;
	FeatureTitle: React.FC;
	LinksContainer: React.FC;
	Summary: React.FC;
	Text: React.FC;
	Title: React.FC;
}

const Cart: React.SFC<CartProps> & CartComposition = ({
	children,
	...restProps
}) => <Container {...restProps}>{children}</Container>;

export const CartInner: React.FC = ({ children, ...restProps }) => (
	<Inner {...restProps}>{children}</Inner>
);

export const CartGroup: React.FC = ({ children, ...restProps }) => (
	<Group {...restProps}>{children}</Group>
);

export const CartTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

export const CartText: React.FC = ({ children, ...restProps }) => (
	<Text {...restProps}>{children}</Text>
);

export const CartBreak: React.FC = ({ ...restProps }) => (
	<Break {...restProps} />
);

export const CartSummary: React.FC = ({ children, ...restProps }) => (
	<Summary {...restProps}>{children}</Summary>
);

export const CartSummaryContainer: React.FC = ({ children, ...restProps }) => (
	<SummaryLinksContainer {...restProps}>{children}</SummaryLinksContainer>
);

export const CartFeature: React.FC = ({ children, ...restProps }) => (
	<Feature {...restProps}>{children}</Feature>
);

export const CartFeatureImage: React.FC<ImageProps> = ({
	src,
	...restProps
}) => <FeatureImage src={src} {...restProps} />;

export const CartFeatureContent: React.FC = ({ children, ...restProps }) => (
	<FeatureContent {...restProps}>{children}</FeatureContent>
);

export const CartFeatureTitle: React.FC = ({ children, ...restProps }) => (
	<FeatureTitle {...restProps}>{children}</FeatureTitle>
);

Cart.Break = CartBreak;
Cart.Group = CartGroup;
Cart.Inner = CartInner;
Cart.Feature = CartFeature;
Cart.FeatureImage = CartFeatureImage;
Cart.FeatureContent = CartFeatureContent;
Cart.FeatureTitle = CartFeatureTitle;
Cart.LinksContainer = CartSummaryContainer;
Cart.Summary = CartSummary;
Cart.Title = CartTitle;
Cart.Text = CartText;

export default Cart;
