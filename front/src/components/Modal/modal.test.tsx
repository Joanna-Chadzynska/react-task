import '@testing-library/jest-dom/extend-expect';
import { fireEvent, render, screen } from '@testing-library/react';
import Theme from 'assets/styles/theme';
import { Modal } from 'components';
import React from 'react';
import { MemoryRouter } from 'react-router-dom';

describe('<Modal/>', () => {
	it('renders the <Modal/> component with populated data', () => {
		const modalRoot = document.createElement('div');
		modalRoot.setAttribute('id', 'modal');
		document.body.appendChild(modalRoot);

		const { getByTestId, baseElement, queryByText } = render(
			<MemoryRouter>
				<Theme>
					<Modal onClose={() => {}}>
						<Modal.Title>Dziękujemy za złożenie zamówienia!</Modal.Title>
						<Modal.Features>
							<Modal.Group>
								<Modal.GroupTitle>Podsumowanie zamówienia:</Modal.GroupTitle>
								<Modal.Text>Zamówienie nr: 1475</Modal.Text>
								<Modal.Text>Wartość zamówienia: 129,90 zł</Modal.Text>
							</Modal.Group>
							<Modal.Group>
								<Modal.GroupTitle>Zamawiający:</Modal.GroupTitle>
								<Modal.Text>Imię i nazwisko: Jan Kowalski</Modal.Text>
								<Modal.Text>Adres: 00-000 Warszawa</Modal.Text>
							</Modal.Group>
						</Modal.Features>
						<Modal.Button to='/somewhere'>Wróć do strony głównej</Modal.Button>
					</Modal>
				</Theme>
			</MemoryRouter>
		);

		expect(
			screen.queryByText('Dziękujemy za złożenie zamówienia!')
		).toBeInTheDocument();

		expect(queryByText('Podsumowanie zamówienia:')).toBeTruthy();
		expect(getByTestId('close-modal-btn')).toBeTruthy();
		fireEvent.click(getByTestId('close-modal-btn'));
		expect(baseElement).toMatchSnapshot();

		document.body.removeChild(modalRoot);
	});
});
