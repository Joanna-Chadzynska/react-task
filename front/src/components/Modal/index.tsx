import React from 'react';
import { createPortal } from 'react-dom';
import {
	Close,
	Container,
	Content,
	FeaturesContainer,
	Group,
	GroupTitle,
	Inner,
	ItemLink,
	Text,
	Title,
} from './styles/modal';

export interface ModalProps {
	onClose?: () => void;
}

export interface ButtonProps {
	to?: any;
}

export interface ModalComposition {
	Button: React.FC<ButtonProps>;
	Features: React.FC;
	Group: React.FC;
	GroupTitle: React.FC;
	Title: React.FC;
	Text: React.FC;
}

const Modal: React.SFC<ModalProps> & ModalComposition = ({
	children,
	onClose,
	...restProps
}) => {
	return createPortal(
		<Container data-testid='my-portal' onClick={onClose} {...restProps}>
			<Inner>
				<Content>{children}</Content>

				<Close onClick={onClose} data-testid='close-modal-btn' />
			</Inner>
		</Container>,
		document.body
	);
};

export const ModalButton: React.FC<ButtonProps> = ({
	to,
	children,
	...restProps
}) => (
	<ItemLink to={to} data-testid={to} {...restProps}>
		{children}
	</ItemLink>
);

export const ModalText: React.FC = ({ children, ...restProps }) => (
	<Text {...restProps}>{children}</Text>
);

export const ModalTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

export const ModalGroup: React.FC = ({ children, ...restProps }) => (
	<Group {...restProps}>{children}</Group>
);

export const ModalGroupTitle: React.FC = ({ children, ...restProps }) => (
	<GroupTitle {...restProps}>{children}</GroupTitle>
);

export const ModalFeatures: React.FC = ({ children, ...restProps }) => (
	<FeaturesContainer {...restProps}>{children}</FeaturesContainer>
);

Modal.Button = ModalButton;
Modal.Features = ModalFeatures;
Modal.Group = ModalGroup;
Modal.GroupTitle = ModalGroupTitle;
Modal.Text = ModalText;
Modal.Title = ModalTitle;

export default Modal;
