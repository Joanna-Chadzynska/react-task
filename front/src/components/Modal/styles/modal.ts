import { ButtonLink } from 'components/Button/styles/button';
import { Link } from 'react-router-dom';
import styled from 'styled-components/macro';

export const Container = styled.div`
	background: rgba(0, 0, 0, 0.5);
	display: flex;
	flex-direction: column;
	justify-content: center;
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 1000;
`;

export const Inner = styled.div`
	background-color: #ffffff;
	height: auto;
	width: 100%;
	max-width: 1000px;
	margin: 0 auto;
	position: relative;
	overflow: auto;
`;

export const Content = styled.div`
	margin: 0 1em 2em 1em;
	display: flex;
	flex-direction: column;
	${ButtonLink} {
		align-self: center;
		margin-top: 1em;
	}
`;

export const ItemLink = styled(Link)`
	align-self: center;
	background-color: ${({ theme }) => theme.colors.orange};
	border-radius: 5px;
	color: #ffffff;
	font-size: 1rem;
	margin-top: 1em;
	padding: 0.5em 1.2em;
	text-transform: uppercase;
	transition: background-color 0.3s ease-in-out;

	&:hover {
		background-color: ${({ theme }) => theme.colors.yellow};
	}
`;

export const Group = styled.div`
	display: flex;
	flex-direction: column;
	gap: 0.25em;
	margin-bottom: 1em;
`;

export const GroupTitle = styled.h3`
	font-weight: 600;
`;

export const FeaturesContainer = styled.div`
	display: grid;
	grid-template-columns: 1fr;
	gap: 1em;

	@media screen and (min-width: 600px) {
		grid-template-columns: repeat(auto-fit, minmax(300px, 1fr));
	}
`;

export const Text = styled.p``;

export const Title = styled.h1`
	margin: 1em 0;
	text-align: center;
`;

export const Close = styled.button`
	position: absolute;
	right: 15px;
	top: 15px;
	width: 22px;
	height: 22px;
	opacity: 0.3;
	background-color: transparent;
	border: 0;
	cursor: pointer;

	&:hover {
		opacity: 1;
	}

	&:before,
	&:after {
		position: absolute;
		left: 10px;
		top: 0;
		content: ' ';
		height: 22px;
		width: 2px;
		background-color: #333;
	}

	&:before {
		transform: rotate(45deg);
	}
	&:after {
		transform: rotate(-45deg);
	}
`;
