import axios, { AxiosInstance } from 'axios';
import { Order } from 'models/interfaces/Order';
import { OrdersResponse } from 'models/interfaces/Response';

declare module 'axios' {
	interface AxiosResponse<T = any> extends Promise<T> {}
}

export class HttpClient {
	protected readonly request: AxiosInstance;

	public constructor(baseURL: string) {
		this.request = axios.create({ baseURL });
	}

	public async getBooks<BooksResponse>(
		chunkNumber: number = 1
	): Promise<BooksResponse> {
		try {
			const response = (await this.request.get(`book/?page=${chunkNumber}`))
				.data;
			return response;
		} catch (error) {
			return error;
		}
	}

	public async searchBook<BooksResponse>(
		typeSearch: string,
		query: string
	): Promise<BooksResponse> {
		try {
			let response;

			if (typeSearch === 'title') {
				response = (await this.request.get(`book?search[title]=${query}`)).data;
			}
			if (typeSearch === 'author') {
				response = (await this.request.get(`book?search[author]=${query}`))
					.data;
			}

			return response;
		} catch (error) {
			return error;
		}
	}

	public async getBookById<BookResponse>(id: number): Promise<BookResponse> {
		try {
			const response = (await this.request.get(`book/${id}`)).data;
			return response;
		} catch (error) {
			return error;
		}
	}

	public async orderBooks(order: Order): Promise<OrdersResponse> {
		try {
			const response = (await this.request.post('/order', order)).data;
			return response;
		} catch (error) {
			return error;
		}
	}
}
