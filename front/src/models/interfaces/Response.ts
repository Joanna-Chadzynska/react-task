import { Book } from './Book';
import { Orders } from './Order';

export interface BooksResponse {
	data: Book[];
	metadata: Metadata;
}

export interface OrdersResponse {
	data: Orders;
}

export interface BookResponse {
	data: Book;
}

export interface Metadata {
	page: number;
	records_per_page: number;
	total_records: number;
}

export interface ErrorResponse {
	error: Error;
}

export interface Error {
	message: string;
	violations: Violations;
}

export interface Violations {
	first_name?: string;
	last_name?: string;
	city?: string;
	zip_code?: string;
}
