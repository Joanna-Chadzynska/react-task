export interface Book {
	/**
	 * The book id
	 */
	id: number;
	/**
	 * Book title
	 */
	title: string;
	/**
	 * Book authors comma separated
	 */
	author: string;
	/**
	 * Full path to image with cover
	 */
	cover_url: string;
	/**
	 * Count of pages in book
	 */
	pages: number;
	/**
	 * Book price set in cents
	 */
	price: number;
	/**
	 * Currency codename (ISO 4217)
	 */
	currency: string;
}
