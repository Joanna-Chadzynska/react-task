export interface Order {
	/**
	 * Array of ordered books
	 */
	order: OrderElement[];
	/**
	 * First name
	 */
	first_name: string;
	/**
	 * Last name
	 */
	last_name: string;
	/**
	 * city
	 */
	city: string;
	/**
	 * Zip code (00-000 format)
	 */
	zip_code: string;
}

export interface OrderElement {
	/**
	 * The book id
	 */
	id: number;
	/**
	 * Quantity
	 */
	quantity: number;
}

export interface Orders extends Order {
	id: number;
}
