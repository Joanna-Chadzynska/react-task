import { RootState } from 'app/store';
import { ReactComponent as Basket } from 'assets/icons/shopping-bag.svg';
import { Header } from 'components';
import * as ROUTES from 'constants/routes';
import React from 'react';
import { useSelector } from 'react-redux';

export interface NavigationProps {}

const Navigation: React.SFC<NavigationProps> = () => {
	const cart = useSelector((state: RootState) => state.cart.cart);
	return (
		<>
			<Header.Logo />

			<Header.Input type='checkbox' />

			<Header.Menu>
				<Header.Group>
					<Header.MenuItem>
						<Header.Link to={ROUTES.HOME}>Strona główna</Header.Link>
					</Header.MenuItem>
					<Header.MenuItem>
						<Header.Link to={ROUTES.SIGN_UP}>Załóż konto</Header.Link>
					</Header.MenuItem>
					<Header.MenuItem>
						<Header.Link to={ROUTES.SIGN_IN}>Zaloguj się</Header.Link>
					</Header.MenuItem>
					<Header.MenuItem>
						<Header.Link to={ROUTES.CART}>
							<Basket />
							<Header.Badge>{cart.length}</Header.Badge>
						</Header.Link>
					</Header.MenuItem>
				</Header.Group>
			</Header.Menu>

			<Header.Burger />
		</>
	);
};

export default Navigation;
