import React from 'react';
import { OrderForm } from './components';

export interface OrderContainerProps {}

const OrderContainer: React.SFC<OrderContainerProps> = () => {
	return (
		<section>
			<OrderForm />
		</section>
	);
};

export default OrderContainer;
