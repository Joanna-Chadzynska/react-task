import { RootState } from 'app/store';
import { Button, Modal } from 'components';
import * as ROUTES from 'constants/routes';
import useCartSummary from 'hooks/useCartSummary';
import React from 'react';
import { useSelector } from 'react-redux';
import OrderSummaryTable from './OrderSummaryTable';

export interface ModalProps {
	onClose: () => void;
}

const OrderSummaryModal: React.SFC<ModalProps> = ({ onClose }) => {
	const order = useSelector((state: RootState) => state.order.order);

	const { booksPriceWithCurrency, orderedBooks } = useCartSummary(order.order);

	return (
		<Modal onClose={onClose}>
			<Modal.Title>Dziękujemy za złożenie zamówienia!</Modal.Title>

			{order && (
				<>
					<Modal.Features>
						<Modal.Group>
							<Modal.GroupTitle>Podsumowanie zamówienia:</Modal.GroupTitle>
							<Modal.Text>Zamówienie nr: {order.id}</Modal.Text>
							<Modal.Text>
								Wartość zamówienia: {booksPriceWithCurrency}
							</Modal.Text>
						</Modal.Group>
						<Modal.Group>
							<Modal.GroupTitle>Zamawiający:</Modal.GroupTitle>
							<Modal.Text>
								Imię i nazwisko: {`${order.first_name} ${order.last_name}`}
							</Modal.Text>
							<Modal.Text>
								Adres: {`${order.zip_code} ${order.city}`}
							</Modal.Text>
						</Modal.Group>
					</Modal.Features>

					<OrderSummaryTable
						orderedBooks={orderedBooks}
						booksPriceWithCurrency={booksPriceWithCurrency}
					/>
				</>
			)}

			<Button.Link to={ROUTES.HOME}>Wróć do strony głównej</Button.Link>
		</Modal>
	);
};

export default OrderSummaryModal;
