import { RootState } from 'app/store';
import { Form } from 'components';
import { useHttpClient } from 'contexts/HttpContext';
import { useModalContext } from 'contexts/modalContext';
import { resetCart } from 'features/cart/cartSlice';
import { sendOrderFailure, sendOrderSuccess } from 'features/order/orderSlice';
import { setUserOrders } from 'features/user/userSlice';
import { validateForm } from 'helpers/validateForm';
import { Order } from 'models/interfaces/Order';
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import OrderSummaryModal from './OrderSummaryModal';

const OrderForm: React.SFC = () => {
	const client = useHttpClient();
	const dispatch = useDispatch();
	const { setShowModal, showModal } = useModalContext();
	const cart = useSelector((state: RootState) => state.cart.cart);
	const errors = useSelector((state: RootState) => state.order.error);

	const [order, setOrder] = useState<Order>({
		order: cart,
		first_name: '',
		last_name: '',
		city: '',
		zip_code: '',
	});

	const [isSubmitted, setIsSubmitted] = useState(false);

	const isInvalid =
		order.first_name === '' ||
		order.last_name === '' ||
		order.city === '' ||
		order.zip_code === '';

	const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
		e.persist();
		const { name, value } = e.target;
		setOrder({
			...order,
			[name]: value,
		});
	};

	const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		setIsSubmitted(true);
		dispatch(sendOrderFailure(validateForm(order)));
	};

	const addOrder = useCallback(async (order: Order) => {
		try {
			const resp = await client.orderBooks(order);
			dispatch(sendOrderSuccess(resp));
			dispatch(setUserOrders(resp.data));
		} catch (error) {
			console.log(error);
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const toggleCloseModal = () => {
		if (setShowModal) {
			setShowModal(false);
			window.location.reload();
		}
	};

	useEffect(() => {
		if (Object.keys(errors).length === 0 && isSubmitted) {
			dispatch(resetCart());
			setOrder({
				...order,
				first_name: '',
				last_name: '',
				city: '',
				zip_code: '',
			});
			addOrder(order);
			if (setShowModal) {
				setShowModal(true);
			}
		}
		return () => {
			setIsSubmitted(false);
		};
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isSubmitted, order, addOrder, errors]);

	return (
		<>
			<Form>
				<Form.Title>Złóż zamówienie</Form.Title>

				<Form.Base onSubmit={handleSubmit}>
					<Form.Input
						type='text'
						name='first_name'
						value={order.first_name || ''}
						onChange={handleChange}
						placeholder='Imię'
						hasError={!!errors.first_name}
					/>
					{errors.first_name && <Form.Error>{errors.first_name}</Form.Error>}

					<Form.Input
						type='text'
						name='last_name'
						value={order.last_name || ''}
						onChange={handleChange}
						placeholder='Nazwisko'
						hasError={!!errors.last_name}
					/>
					{errors.last_name && <Form.Error>{errors.last_name}</Form.Error>}

					<Form.Input
						type='text'
						name='city'
						value={order.city || ''}
						onChange={handleChange}
						placeholder='Miasto'
						hasError={!!errors.city}
					/>
					{errors.city && <Form.Error>{errors.city}</Form.Error>}

					<Form.Input
						type='text'
						name='zip_code'
						value={order.zip_code || ''}
						onChange={handleChange}
						placeholder='Kod pocztowy'
						hasError={!!errors.zip_code}
					/>
					{errors.zip_code && <Form.Error>{errors.zip_code}</Form.Error>}

					<Form.Submit disabled={isInvalid} type='submit'>
						Złóż zamówienie
					</Form.Submit>
				</Form.Base>
			</Form>

			{showModal && <OrderSummaryModal onClose={toggleCloseModal} />}
		</>
	);
};

export default OrderForm;
