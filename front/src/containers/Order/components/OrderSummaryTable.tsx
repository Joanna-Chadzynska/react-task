import { Table } from 'components';
import React from 'react';

export interface OrderSummaryTableProps {
	booksPriceWithCurrency: string;
	orderedBooks: any;
}

const OrderSummaryTable: React.SFC<OrderSummaryTableProps> = ({
	booksPriceWithCurrency,
	orderedBooks,
}) => {
	return (
		<Table>
			<Table.Caption>Zakupione produkty</Table.Caption>
			<Table.Header>
				<Table.Row>
					<Table.HeaderItem scope='col'>Tytuł</Table.HeaderItem>
					<Table.HeaderItem scope='col'>Autor</Table.HeaderItem>
					<Table.HeaderItem scope='col'>Ilość</Table.HeaderItem>
					<Table.HeaderItem scope='col'>Cena</Table.HeaderItem>
					<Table.HeaderItem scope='col'>Wartość</Table.HeaderItem>
				</Table.Row>
			</Table.Header>
			<Table.Body>
				{orderedBooks &&
					orderedBooks.map((book: any) => (
						<Table.Row key={book.id}>
							<Table.BodyItem>{book.title}</Table.BodyItem>
							<Table.BodyItem>{book.author}</Table.BodyItem>
							<Table.BodyItem>{book.quantity}szt.</Table.BodyItem>
							<Table.BodyItem align='right'>
								{(book.price / 100).toFixed(2).replace('.', ',')}
								{book.currency === 'PLN' && 'zł'}
							</Table.BodyItem>
							<Table.BodyItem align='right'>{`${book.value} ${
								book.currency === 'PLN' && 'zł'
							}`}</Table.BodyItem>
						</Table.Row>
					))}
			</Table.Body>
			<Table.Footer>
				<Table.Row>
					<Table.BodyItem align='right' colSpan={5}>
						Razem: <strong>{booksPriceWithCurrency}</strong>
					</Table.BodyItem>
				</Table.Row>
			</Table.Footer>
		</Table>
	);
};

export default OrderSummaryTable;
