import { RootState } from 'app/store';
import React from 'react';
import { useSelector } from 'react-redux';
import { CartEmpty, CartListing } from './components';

const CartContainer: React.SFC = () => {
	const cart = useSelector((state: RootState) => state.cart);

	if (cart.loading) return <div>loading...</div>;
	if (cart.error) return <div>error in cart </div>;

	return <>{cart.cart.length > 0 ? <CartListing /> : <CartEmpty />}</>;
};

export default CartContainer;
