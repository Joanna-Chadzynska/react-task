/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from 'app/store';
import { Button, Cart } from 'components';
import * as ROUTES from 'constants/routes';
import { useHttpClient } from 'contexts/HttpContext';
import { getAllBooks } from 'features/books/booksSlice';
import { BooksResponse } from 'models/interfaces/Response';
import React, { useCallback, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useCartSummary from './../../../hooks/useCartSummary';
import CartItem from './CartItem';
import CartTitle from './CartTitle';

const CartListing: React.SFC = () => {
	const client = useHttpClient();
	const dispatch = useDispatch();
	const cart = useSelector((state: RootState) => state.cart.cart);
	const { booksPriceWithCurrency } = useCartSummary(cart);
	const books = useSelector((state: RootState) => state.books);

	const maxPage = Math.ceil(
		books.metadata.total_records / books.metadata.records_per_page
	);

	const totalPages = Array.from({ length: maxPage }, (_, i) => i + 1);

	const fetchAllBooks = useCallback(async () => {
		let books: any = [];
		for (let i of totalPages) {
			const resp: BooksResponse = await client.getBooks(i);
			books.push(resp.data);
		}
		dispatch(getAllBooks(books.flat()));
	}, [totalPages]);

	useEffect(() => {
		fetchAllBooks();
		return () => {};
	}, []);

	return (
		<Cart>
			<CartTitle />
			<hr />
			<Cart.Group>
				{cart.map((cart) => (
					<CartItem key={cart.id} {...cart} />
				))}
			</Cart.Group>

			<Cart.Summary>
				<Cart.Title>PODSUMOWANIE</Cart.Title>
				<hr />
				<br />
				<Cart.Text>
					DO ZAPŁATY: <strong>{booksPriceWithCurrency}</strong>
				</Cart.Text>
				<Cart.LinksContainer>
					<Button.Link to={ROUTES.HOME}>Wróć na stronę główną</Button.Link>
					<Button.Link to={ROUTES.ORDER}>Zamawiam i płacę</Button.Link>
				</Cart.LinksContainer>
			</Cart.Summary>
		</Cart>
	);
};

export default CartListing;
