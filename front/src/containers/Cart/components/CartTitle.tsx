import { RootState } from 'app/store';
import { Cart } from 'components';
import * as React from 'react';
import { useSelector } from 'react-redux';
export interface CartTitleProps {}

const CartTitle: React.SFC<CartTitleProps> = () => {
	const cart = useSelector((state: RootState) => state.cart.cart);
	const countItems = cart
		.map((item) => item.quantity)
		.reduce((a, b) => a + b, 0);

	return (
		<Cart.Title>
			Mój koszyk &nbsp;
			<Cart.Text>
				({cart.length} produkt, {countItems} sztuka)
			</Cart.Text>
		</Cart.Title>
	);
};

export default CartTitle;
