/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from 'app/store';
import remove from 'assets/icons/remove_shopping_cart.svg';
import { Button, Card, Cart } from 'components';
import { removeBookFromCart, updateQuantity } from 'features/cart/cartSlice';
import { OrderElement } from 'models/interfaces/Order';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

const CartItem: React.SFC<OrderElement> = (props) => {
	const dispatch = useDispatch();
	const books = useSelector((state: RootState) => state.books);
	const book = books.books.filter((book) => book.id === props.id);
	const [quantity, setQuantity] = useState(props.quantity);

	const handleQuantity = (e: React.ChangeEvent<HTMLInputElement>) =>
		setQuantity(Number(e.target.value));

	const handleRemoveItem = (id: OrderElement) =>
		dispatch(removeBookFromCart(id));

	useEffect(() => {
		dispatch(updateQuantity({ id: props.id, quantity }));
		return () => {};
	}, [quantity]);

	return (
		<>
			{book.map((book) => (
				<Cart.Feature key={book.id}>
					<Cart.FeatureImage src={book.cover_url} />

					<Cart.FeatureContent>
						<Cart.FeatureTitle>{book.title}</Cart.FeatureTitle>
						<Card.Author>{book.author}</Card.Author>
						<Card.Text>Strony: {book.pages}</Card.Text>
						<Card.PriceWrapper>
							<Card.Price>
								{(book.price / 100).toFixed(2).replace('.', ',')}
							</Card.Price>
							<Card.Currency>{book.currency === 'PLN' && 'zł'}</Card.Currency>
						</Card.PriceWrapper>

						<Card.AddToCart>
							<Card.Input
								type='number'
								value={quantity}
								onChange={handleQuantity}
							/>
							<Button onClick={() => handleRemoveItem(props)}>
								Usuń z koszyka
								<img src={remove} alt='remove from cart icon' />
							</Button>
						</Card.AddToCart>
					</Cart.FeatureContent>
				</Cart.Feature>
			))}
		</>
	);
};

export default CartItem;
