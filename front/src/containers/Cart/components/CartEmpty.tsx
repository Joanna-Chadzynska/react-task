import { Button, Cart } from 'components';
import * as ROUTES from 'constants/routes';
import React from 'react';

const CartEmpty: React.SFC = () => (
	<Cart>
		<Cart.Inner>
			<Cart.Title>Twój koszyk jest pusty</Cart.Title>
			<Button.Link to={ROUTES.HOME}>Przejdź do strony głównej</Button.Link>
		</Cart.Inner>
	</Cart>
);

export default CartEmpty;
