import { Footer, Header } from 'containers';
import React from 'react';

export interface LayoutProps {
	home?: boolean;
	notFound?: boolean;
}

const Layout: React.SFC<LayoutProps> = ({ children, home, notFound }) => {
	return (
		<>
			{/* <Header>{home && <Jumbotron />}</Header> */}
			<Header />
			<main>{children}</main>
			<Footer />
		</>
	);
};

export default Layout;
