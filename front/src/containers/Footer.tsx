import { Footer } from 'components';
import React from 'react';

const LINKS = [
	{ id: 'github', path: 'https://github.com/Joanna-Chadzynska' },
	{ id: 'gitlab', path: 'https://gitlab.com/Joanna-Chadzynska/react-task' },
	{
		id: 'linkedin',
		path: 'https://www.linkedin.com/in/joanna-ch%C4%85dzy%C5%84ska/',
	},
];

const FooterContainer: React.SFC = () => (
	<Footer>
		<span>Joanna Chądzyńska © {new Date().getFullYear()}</span>
		<Footer.Group>
			{LINKS.map((link) => (
				<Footer.Link key={link.id} path={link.path} icon={link.id} />
			))}
		</Footer.Group>
	</Footer>
);

export default FooterContainer;
