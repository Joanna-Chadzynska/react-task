import { Header } from 'components';
import React from 'react';
import Navigation from './Navigation';

const HeaderContainer: React.SFC = ({ children }) => {
	return (
		<Header>
			<Header.Inner>
				<Navigation />
			</Header.Inner>
			{children}
		</Header>
	);
};

export default HeaderContainer;
