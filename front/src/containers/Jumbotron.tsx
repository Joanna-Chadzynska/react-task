import { Button, Jumbotron } from 'components';
import React from 'react';

export interface JumbotronContainerProps {}

const JumbotronContainer: React.SFC<JumbotronContainerProps> = () => {
	return (
		<Jumbotron>
			<Jumbotron.Content>
				<Jumbotron.Title>It Starts Here</Jumbotron.Title>
				<Jumbotron.SubTitle>
					Lorem ipsum dolor sit amet consectetur adipisicing elit.
				</Jumbotron.SubTitle>
				<Button.Link>Przeglądaj książki</Button.Link>
			</Jumbotron.Content>
			<Jumbotron.Image />
		</Jumbotron>
	);
};

export default JumbotronContainer;
