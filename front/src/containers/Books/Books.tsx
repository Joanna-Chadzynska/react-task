/* eslint-disable react-hooks/exhaustive-deps */
import { RootState } from 'app/store';
import { Card } from 'components';
import { useHttpClient } from 'contexts/HttpContext';
import { getBooksSuccess } from 'features/books/booksSlice';
import { BooksResponse } from 'models/interfaces/Response';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import usePagination from './../../hooks/usePagination';
import { Book, Paging, SearchBooks } from './components';

const Books: React.SFC = () => {
	const client = useHttpClient();
	const dispatch = useDispatch();
	const books = useSelector((state: RootState) => state.books);
	const [searchTerm, setSearchTerm] = useState('');
	const [searchType, setSearchType] = useState('title');

	const { currentPage, next, prev, jump, maxPage, totalPages } = usePagination(
		books.metadata
	);

	const changeSearchType = (e: React.ChangeEvent<HTMLInputElement>) => {
		setSearchType(e.target.value);
	};

	const searchBook = async (e: React.ChangeEvent<HTMLInputElement>) => {
		const term = `${e.target.value
			.charAt(0)
			.toUpperCase()}${e.target.value.slice(1)}`;

		setSearchTerm(term);
		const resp: BooksResponse = await client.searchBook(searchType, term);
		dispatch(getBooksSuccess(resp));
	};

	const fetchBooks = async (currentPage: number) => {
		const resp: BooksResponse = await client.getBooks(currentPage);
		dispatch(getBooksSuccess(resp));
	};

	React.useEffect(() => {
		fetchBooks(currentPage);
	}, [currentPage]);

	return (
		<Card.GroupContainer>
			<SearchBooks
				searchType={searchType}
				searchTerm={searchTerm}
				searchBook={searchBook}
				changeSearchType={changeSearchType}
			/>

			<Card.Group>
				{books.books.map((book) => (
					<Book key={book.id} {...book} />
				))}
			</Card.Group>

			<Paging
				currentPage={currentPage}
				maxPage={maxPage}
				totalPages={totalPages}
				next={next}
				prev={prev}
				jump={jump}
			/>
		</Card.GroupContainer>
	);
};

export default Books;
