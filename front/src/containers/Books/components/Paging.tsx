import { Pagination } from 'components';
import React from 'react';

export interface PagingProps {
	currentPage: number;
	maxPage: number;
	totalPages: number[];
	next: () => void;
	prev: () => void;
	jump: (page: number) => void;
}

const Paging: React.SFC<PagingProps> = ({
	currentPage,
	maxPage,
	totalPages,
	next,
	prev,
	jump,
}) => {
	return (
		<Pagination>
			<Pagination.Button onClick={prev} disabled={currentPage === 1}>
				Prev
			</Pagination.Button>
			{totalPages.map((page, index) => (
				<Pagination.Button
					key={index}
					onClick={() => jump(page)}
					active={currentPage === page}>
					{page}
				</Pagination.Button>
			))}

			<Pagination.Button onClick={next} disabled={currentPage === maxPage}>
				Next
			</Pagination.Button>
		</Pagination>
	);
};

export default Paging;
