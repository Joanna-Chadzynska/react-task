import add from 'assets/icons/add_cart.svg';
import { Button, Card } from 'components';
import { addBookToCart } from 'features/cart/cartSlice';
import { Book as IBook } from 'models/interfaces/Book';
import React from 'react';
import { useDispatch } from 'react-redux';

const BookItem: React.SFC<IBook> = (props) => {
	const dispatch = useDispatch();
	const { author, title, pages, price, currency, cover_url, id } = props;

	const handleAddToCart = () =>
		dispatch(addBookToCart({ id: id, quantity: 1 }));

	return (
		<Card>
			<Card.Content>
				<Card.Image src={cover_url} alt={`${title} book cover`} />

				<Card.Description>
					<Card.Title>{title}</Card.Title>
					<Card.Author>{author}</Card.Author>
					<Card.Text>Strony: {pages}</Card.Text>
					<Card.PriceWrapper>
						<Card.Price>
							{(price / 100).toFixed(2).replace('.', ',')}
						</Card.Price>
						<Card.Currency>{currency === 'PLN' && 'zł'}</Card.Currency>
					</Card.PriceWrapper>
				</Card.Description>
			</Card.Content>

			<Button onClick={handleAddToCart}>
				Do koszyka
				<img src={add} alt='add to cart icon' />
			</Button>
		</Card>
	);
};

export default BookItem;
