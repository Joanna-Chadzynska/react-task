import { Card } from 'components';
import React from 'react';

export interface SearchBooksProps {
	searchType: string;
	changeSearchType: (e: React.ChangeEvent<HTMLInputElement>) => void;
	searchBook: (e: React.ChangeEvent<HTMLInputElement>) => void;
	searchTerm: string;
}

const SearchBooks: React.SFC<SearchBooksProps> = ({
	searchType,
	changeSearchType,
	searchBook,
	searchTerm,
}) => {
	return (
		<Card.Search
			type='search'
			onChange={searchBook}
			value={searchTerm}
			placeholder={
				searchType === 'author' ? 'Szukaj wg autora' : 'Szukaj wg tytułu'
			}
			searchType={searchType}>
			<div>
				<Card.Input
					type='radio'
					id='title'
					name='query'
					value='title'
					checked={searchType === 'title'}
					onChange={changeSearchType}
				/>
				<label htmlFor='title'>tytuł</label>
			</div>
			<div>
				<Card.Input
					type='radio'
					id='author'
					name='query'
					value='author'
					checked={searchType === 'author'}
					onChange={changeSearchType}
				/>
				<label htmlFor='author'>autor</label>
			</div>
		</Card.Search>
	);
};

export default SearchBooks;
