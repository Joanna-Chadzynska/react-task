import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Book } from 'models/interfaces/Book';
import { BooksResponse } from 'models/interfaces/Response';
import { BookResponse } from './../../models/interfaces/Response';

export interface Metadata {
	page: number;
	records_per_page: number;
	total_records: number;
}

interface BooksState {
	book: Book;
	books: Book[];
	error: string | null;
	loading: boolean;
	metadata: Metadata;
}

const initialState: BooksState = {
	book: {} as Book,
	books: [],
	error: null,
	loading: false,

	metadata: {} as Metadata,
};

function startLoading(state: BooksState) {
	state.loading = true;
}

function loadingFailed(
	state: BooksState,
	action: PayloadAction<string | null>
) {
	state.loading = false;
	state.error = action.payload;
}

export const booksSlice = createSlice({
	name: 'books',
	initialState,
	reducers: {
		getBooksStart: startLoading,
		getBooksSuccess: (state, { payload }: PayloadAction<BooksResponse>) => {
			const { data, metadata } = payload;

			state.books = data;
			state.loading = false;
			state.error = null;
			state.metadata = metadata;
		},
		getBooksFailure: loadingFailed,
		getBookDetails: (state, { payload }: PayloadAction<BookResponse>) => {
			state.book = payload.data;
			state.loading = false;
			state.error = null;
		},
		getAllBooks: (state, { payload }: PayloadAction<Book[]>) => {
			state.books = payload;
			state.loading = false;
			state.error = null;
		},
	},
});

export const {
	getBooksSuccess,
	getBooksFailure,
	getBooksStart,
	getBookDetails,
	getAllBooks,
} = booksSlice.actions;

export default booksSlice.reducer;
