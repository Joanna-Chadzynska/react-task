import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Orders } from 'models/interfaces/Order';
import { OrdersResponse, Violations } from 'models/interfaces/Response';

export interface Metadata {
	page: number;
	records_per_page: number;
	total_records: number;
}

interface orderState {
	order: Orders;
	error: Violations;
	loading: boolean;
}

const initialState: orderState = {
	order: {} as Orders,
	error: {} as Violations,
	loading: true,
};

export const orderSlice = createSlice({
	name: 'order',
	initialState,
	reducers: {
		sendOrderSuccess: (
			state,
			{ payload: { data } }: PayloadAction<OrdersResponse>
		) => {
			state.order = data;
			state.loading = false;
			state.error = {} as Violations;
		},
		sendOrderFailure: (state, { payload }: PayloadAction<Violations>) => {
			state.order = {} as Orders;
			state.error = payload;
			state.loading = false;
		},
		resetOrderErrors: (state) => {
			state.error = {} as Violations;
		},
	},
});

export const {
	sendOrderSuccess,
	sendOrderFailure,
	resetOrderErrors,
} = orderSlice.actions;

export default orderSlice.reducer;
