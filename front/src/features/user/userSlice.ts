import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { Orders } from 'models/interfaces/Order';

interface CartState {
	orders: Orders[];
	error: string | null;
	loading: boolean;
}

const initialState: CartState = {
	orders: [],
	error: null,
	loading: false,
};

export const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		setUserOrders: (state, { payload }: PayloadAction<Orders>) => {
			state.orders = [...state.orders, payload];
		},
	},
});

export const { setUserOrders } = userSlice.actions;

export default userSlice.reducer;
