import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { OrderElement } from 'models/interfaces/Order';

export interface Metadata {
	page: number;
	records_per_page: number;
	total_records: number;
}

interface CartState {
	cart: OrderElement[];
	error: string | null;
	loading: boolean;
}

const initialState: CartState = {
	cart: [],
	error: null,
	loading: false,
};

export const cartSlice = createSlice({
	name: 'cart',
	initialState,
	reducers: {
		addBookToCart: (state, { payload }: PayloadAction<OrderElement>) => {
			const { id, quantity } = payload;

			if (!state.cart.some((el) => el.id === id)) {
				state.cart = [...state.cart, payload];
			} else {
				state.cart
					.filter((item) => item.id === id)
					.map((item) => (item.quantity = quantity + 1));
			}
		},
		removeBookFromCart: (state, { payload }: PayloadAction<OrderElement>) => {
			const { id } = payload;
			state.cart = state.cart.filter((item) => item.id !== id);
		},
		updateQuantity: (state, { payload }: PayloadAction<OrderElement>) => {
			const { id, quantity } = payload;
			state.cart
				.filter((item) => item.id === id)
				.map((item) => (item.quantity = quantity));
		},
		resetCart: (state) => {
			state.cart = [];
			state.error = null;
			state.loading = false;
		},
	},
});

export const {
	addBookToCart,
	removeBookFromCart,
	updateQuantity,
	resetCart,
} = cartSlice.actions;

export default cartSlice.reducer;
