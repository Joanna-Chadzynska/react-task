import React from 'react';
import { HttpClient } from './../models/providers/HttpClient';

const HTTPClientContext = React.createContext(
	new HttpClient('http://localhost:3001/api/')
);

export const useHttpClient = () => {
	const clientInstance = React.useContext(HTTPClientContext);
	return clientInstance;
};
