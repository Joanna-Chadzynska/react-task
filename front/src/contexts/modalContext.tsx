import React, { createContext, useContext, useState } from 'react';

interface Modal {
	showModal: boolean;
	setShowModal: React.Dispatch<React.SetStateAction<boolean>>;
}

const ModalContext = createContext<Partial<Modal>>({});

const ModalProvider: React.SFC = ({ children }) => {
	// eslint-disable-next-line @typescript-eslint/no-unused-vars
	const [showModal, setShowModal] = useState<boolean>(false);
	return (
		<ModalContext.Provider value={{ showModal: showModal, setShowModal }}>
			{children}
		</ModalContext.Provider>
	);
};

const useModalContext = () => {
	const { showModal, setShowModal } = useContext(ModalContext);

	return { showModal, setShowModal };
};

export { ModalContext, ModalProvider, useModalContext };
